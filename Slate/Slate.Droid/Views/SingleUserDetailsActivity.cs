using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using Slate.Droid.Classes;
using Slate.Models;
using Slate.ViewModels;
using System;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "SingleUserDetailsActivity")]
    public class SingleUserDetailsActivity : NavigationDrawerActivity
    {
        Employee employee;
        private string empCode,empId;
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SingleUserDetails);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.EmployeeDetails);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            TextView nameText = FindViewById<TextView>(Resource.Id.NameText);
            TextView cityText = FindViewById<TextView>(Resource.Id.cityText);
            TextView zipText = FindViewById<TextView>(Resource.Id.zipText);
            TextView employeeIdText = FindViewById<TextView>(Resource.Id.employeeIdText);
            TextView roleText = FindViewById<TextView>(Resource.Id.roleText);
            TextView joiningDateText = FindViewById<TextView>(Resource.Id.joiningDateText);
            TextView addressText = FindViewById<TextView>(Resource.Id.addressText);
            TextView emailAddressText = FindViewById<TextView>(Resource.Id.emailAddressText);
            TextView stateText = FindViewById<TextView>(Resource.Id.stateText);
            ImageView profileImage = FindViewById<ImageView>(Resource.Id.profileImage);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            empCode = Intent.GetStringExtra("empcode");
            empId = Intent.GetStringExtra("empId");
            if (AppUtil.IsNetworkAvailable(this))
            {
                ProgressDialog myProgressBar = new ProgressDialog(this);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
                //employee = response.Data;
                addressText.Text = employee.PermanentAddress?.Address;
                cityText.Text = employee.PermanentAddress?.City;
                zipText.Text = employee.PermanentAddress?.Zip;
                stateText.Text = employee.PermanentAddress?.State;
                nameText.Text = employee.FirstName + " " + employee.LastName;
                roleText.Text = employee.JobTitle;
                joiningDateText.Text = Convert.ToDateTime(employee.JoiningDate).ToString("MMM dd, yyyy");
                employeeIdText.Text = "Employee Id : " + employee.EmpCode.ToString();
                emailAddressText.Text = employee.Email;
                ImageLoader imageLoader = ImageLoader.Instance;
                imageLoader.DisplayImage(employee.ProfileImage, profileImage);
                emailAddressText.Click += delegate
                {
                    var email = new Intent(Intent.ActionSend);
                    email.PutExtra(Intent.ExtraEmail, new string[] { employee.Email });
                    email.SetType("message/rfc822");
                    StartActivity(email);
                };

            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.NoInternetConnection), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }
        }

        public async System.Threading.Tasks.Task FetchData()
        {
            EmployeeViewModel employeeViewModel = new EmployeeViewModel();
            var employeeListResponse = await employeeViewModel.EmployeeList(AppPreferences.GetAccessToken(),empId);
            if (employeeListResponse != null)
            {
                employee = employeeListResponse;
            }

        }

    }
}