using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Widget;
using Slate.Droid.Adapters;
using Slate.Droid.Classes;
using Slate.Models;
using Slate.ViewModels;
using System.Collections.Generic;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "HolidayActivity")]
    public class HolidayActivity : NavigationDrawerActivity
    {
        List<Holiday> holidaysList;
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.HolidaysLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Holidays);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            RecyclerView holidayRecyclerView = FindViewById<RecyclerView>(Resource.Id.holidayRecyclerView);

            if (AppUtil.IsNetworkAvailable(this))
            {
                ProgressDialog myProgressBar = new ProgressDialog(this);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
                if (holidaysList != null)
                {
                    RecyclerView.LayoutManager holidayLayoutManager = new LinearLayoutManager(this);
                    holidayRecyclerView.SetLayoutManager(holidayLayoutManager);
                    HolidayAdapter holidayAdapter = new HolidayAdapter(holidaysList, this);
                    holidayRecyclerView.SetAdapter(holidayAdapter);
                }
            }
            else
                Toast.MakeText(this, Resource.String.NoInternetConnection, ToastLength.Long).Show();
        }

        public async System.Threading.Tasks.Task FetchData()
        {
            HolidayViewModel holidayViewModel = new HolidayViewModel();
            var settingListResponse = await holidayViewModel.GetSettings(AppPreferences.GetAccessToken());
            if (settingListResponse != null)
                 holidaysList = settingListResponse?.Holidays;
       }
    }
}
