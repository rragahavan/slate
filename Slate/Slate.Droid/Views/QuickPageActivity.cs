
using Android.App;
using Android.Content;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Widget;
using Slate.Droid.Classes;
using Slate.Models;
//using OfficeTools.Employees.Droid.Classes;
//using OfficeTools.Employees.Droid.Repositories;
//using OfficeTools.Shared.Response;
using System.Collections.Generic;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "QuickPageActivity")]
    public class QuickPageActivity : NavigationDrawerActivity//, GoogleApiClient.IConnectionCallbacks,
                                                             //GoogleApiClient.IOnConnectionFailedListener, Android.Gms.Location.ILocationListener
    {
        private string CheckInDate;
        private List<Attendance> attendance = new List<Attendance>();
        GoogleApiClient apiClient;
        LocationRequest locRequest;
        private string status;
        Button checkInButton;
        CardView checkInCardview;
        bool _isGooglePlayServicesInstalled;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.QuickPageLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.QuickAccess);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            checkInButton = FindViewById<Button>(Resource.Id.checkInButton);
            CardView leaveCardview = FindViewById<CardView>(Resource.Id.leaveCardview);
            CardView claimCardview = FindViewById<CardView>(Resource.Id.claimCardview);
            CardView timesheetCardview = FindViewById<CardView>(Resource.Id.timesheetCardview);
            checkInCardview = FindViewById<CardView>(Resource.Id.checkInCardview);
            //_isGooglePlayServicesInstalled = IsGooglePlayServicesInstalled();
            leaveCardview.Click += delegate
            {
                Intent i = new Intent(this, typeof(LeaveManagementActivity));
                StartActivity(i);
            };
            timesheetCardview.Click += delegate
            {
                Intent i = new Intent(this, typeof(TimesheetActivity));
                StartActivity(i);
            };
            claimCardview.Click += delegate
            {
                Intent i = new Intent(this, typeof(ClaimActivity));
                StartActivity(i);
            };
            if (AppUtil.IsNetworkAvailable(this))
            {
                //var attendanceList =  new EmployeeRepository().GetAttendanceList();

                //if (!attendanceList.Success)
                //    return;
                //else
                //{
                //    attendance = attendanceList.Data;
                //    //local testing
                //    if (attendance != null)
                //    {
                //        if (attendance.Count != 0)
                //        {
                //            var t = attendance[attendance.Count - 1];
                //            if (t.Status.Equals("CheckIn"))
                //                checkInButton.Text = "CheckOut";
                //            else
                //            {
                //                checkInButton.Text = "CheckIn";
                //                CheckInDate = t.CheckIn;
                //            }
                //        }
                //    }

            }
        }

        //if (_isGooglePlayServicesInstalled)
        //{
        //    apiClient = new GoogleApiClient.Builder(this, this, this)
        //        .AddApi(LocationServices.API).Build();
        //    locRequest = new LocationRequest();

        //}
        //else
        //{

        //    Toast.MakeText(this, "Google Play Services is not installed", ToastLength.Long).Show();
        //    Finish();
        //}

    }


    //        public void CheckOutDate()
    //      {

    //var attendanceList = new EmployeeRepository(this).GetAttendanceList();
    //if (attendanceList.Success)
    //{
    //    attendance = attendanceList.Data;
    //    if (attendance.Count != 0)
    //    {
    //        var t = attendance[attendance.Count - 1];
    //        if (t.Status.Equals("CheckIn"))
    //            CheckInDate = t.CheckIn;
    //    }

    //}
    //    }


    //bool IsGooglePlayServicesInstalled()
    //{
    //    int queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
    //    if (queryResult == ConnectionResult.Success)
    //        return true;

    //    if (GoogleApiAvailability.Instance.IsUserResolvableError(queryResult))
    //    {
    //        string errorString = GoogleApiAvailability.Instance.GetErrorString(queryResult);
    //        Toast.MakeText(this, "There is a problem with Google Play Services on this device: {0} - {1}" + errorString, ToastLength.Long).Show();
    //    }
    //    return false;
    //}

    //    protected override void OnResume()
    //    {
    //        base.OnResume();
    //        apiClient.Connect();

    //        checkInCardview.Click += async delegate
    //        {
    //                //bool _isGpsEnabled=false;
    //                //LocationManager _locationManager = (LocationManager)GetSystemService(LocationService);
    //                //if (_locationManager.IsProviderEnabled(LocationManager.GpsProvider))
    //                //{

    //                //    _isGpsEnabled = true;

    //                //}
    //                //else
    //                //{

    //                //    Intent gpsOptionsIntent = new Intent(
    //                //       Android.Provider.Settings.ActionLocationSourceSettings);
    //                //    StartActivity(gpsOptionsIntent);

    //                //}

    //                if (AppUtil.IsNetworkAvailable(this))
    //            {

    //                    //if (_isGpsEnabled)
    //                    //{
    //                    if (apiClient.IsConnected)
    //                {
    //                    if (checkInButton.Text.Equals("CheckIn"))
    //                    {
    //                        checkInButton.Text = "CheckOut";
    //                        status = "CheckIn";

    //                    }
    //                    else
    //                    {
    //                        checkInButton.Text = "CheckIn";
    //                        status = "CheckOut";
    //                        CheckOutDate();
    //                    }

    //                    locRequest.SetPriority(100);
    //                    locRequest.SetFastestInterval(500);
    //                    locRequest.SetInterval(1000);
    //                    await LocationServices.FusedLocationApi.RequestLocationUpdates(apiClient, locRequest, this);
    //                }
    //            }
    //                //else
    //                //  Toast.MakeText(this, Resource.String.NoGpsConnection, ToastLength.Long).Show();
    //                //}
    //                else
    //            {
    //                Toast.MakeText(this, Resource.String.NoInternetConnection, ToastLength.Long).Show();

    //            }

    //        };





    //    }

    //    protected override async void OnPause()
    //    {
    //        base.OnPause();
    //        if (apiClient.IsConnected)
    //        {
    //            await LocationServices.FusedLocationApi.RemoveLocationUpdates(apiClient, this);
    //            apiClient.Disconnect();
    //        }
    //    }


    //    public void OnConnected(Bundle bundle)
    //    {

    //    }

    //    public void OnDisconnected()
    //    {

    //    }

    //    public void OnConnectionFailed(ConnectionResult bundle)
    //    {


    //    }


    //    public void OnLocationChanged(Location location)
    //    {
    //        var locationDatail = new { latitude = location.Latitude, longitude = location.Longitude };
    //        if (AppUtil.IsNetworkAvailable(this))
    //        {
    //            //var response = new EmployeeRepository(this).SaveAttendanceDetails(status, CheckInDate, locationDatail).data;
    //        }
    //        else
    //            Toast.MakeText(this, Resource.String.NoInternetConnection, ToastLength.Long).Show();

    //    }

    //    public void OnConnectionSuspended(int i)
    //    {

    //    }

    //}
}

//}