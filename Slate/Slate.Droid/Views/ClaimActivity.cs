using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Views;
using Android.Widget;
using Slate.Droid.Classes;
using Slate.Droid.Fragments;
using Slate.Models;
using Slate.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "ClaimActivity")]
    public class ClaimActivity : NavigationDrawerActivity
    {

        private ImageView _imageView;
        private List<string> expenseCategoryList = new List<string>();
        private List<string> currencyTypesList = new List<string>();
        private string currencyValue = "", categoryValue = "";
        private EditText amountText, amountCommentText;
        private string amount, amountComment;
        private EditText claimDateText;
        private TextView cameraBillText;
        private Bitmap bitmap;
        private string claimDateValue = "";
        private Button submitApprovalButton;
        private string fileName;
        FinanceManager financeManager;
        private string claimTemplate;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ClaimLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Claim);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            Spinner categorySpinner = FindViewById<Spinner>(Resource.Id.categorySpinner);
            Spinner currencySpinner = FindViewById<Spinner>(Resource.Id.currencySpinner);
            amountText = FindViewById<EditText>(Resource.Id.amountText);
            claimDateText = FindViewById<EditText>(Resource.Id.claimDateText);
            cameraBillText = FindViewById<TextView>(Resource.Id.cameraImageName);
            expenseCategoryList.Add(GetString(Resource.String.ChooseCategory));
            currencyTypesList.Add(GetString(Resource.String.ChooseCurrency));
            claimDateText.Click += ClaimDateText_Click;
            amountCommentText = FindViewById<EditText>(Resource.Id.amountCommentText);
            Button submitButton = FindViewById<Button>(Resource.Id.submitClaimButton);
            submitButton.Click += delegate
            {
                amount = amountText.Text;
                amountComment = amountCommentText.Text;
            };

            cameraBillText.Click += delegate
            {
                if (bitmap != null)
                    loadPhoto(200, 200);
                else
                    Display("Upload or attach bill");
            };


            if (AppUtil.IsNetworkAvailable(this))
            {
                ProgressDialog myProgressBar = new ProgressDialog(this);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
                ArrayAdapter<String> categorySpinnerArrayAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerItem, expenseCategoryList); //selected item will look like a spinner set from XML
                categorySpinnerArrayAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                categorySpinner.ItemSelected += CategorySpinner_ItemSelected;
                categorySpinner.Adapter = categorySpinnerArrayAdapter;
                ArrayAdapter<String> currencySpinnerArrayAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerItem, currencyTypesList); //selected item will look like a spinner set from XML
                currencySpinnerArrayAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                currencySpinner.ItemSelected += CurrencySpinner_ItemSelected;
                currencySpinner.Adapter = currencySpinnerArrayAdapter;
            }

            ImageButton openGalleryButton;

            if (IsThereAnAppToTakePictures())
            {
                CreateDirectoryForPictures();
                ImageButton button = FindViewById<ImageButton>(Resource.Id.myButton);
                _imageView = FindViewById<ImageView>(Resource.Id.imageView1);
                openGalleryButton = FindViewById<ImageButton>(Resource.Id.openGallery);
                submitApprovalButton = FindViewById<Button>(Resource.Id.submitClaimButton);
                openGalleryButton.Click += OpenGalleryButton_Click;
                submitApprovalButton.Click += SubmitApprovalButton_Click;
                button.Click += TakeAPicture;
            }

        }

        private void SubmitApprovalButton_Click(object sender, EventArgs e)
        {
            amount = amountText.Text;
            int amountValue;
            int.TryParse(amount, out amountValue);
            amountComment = amountCommentText.Text;
            if (claimDateValue.Equals(""))
            {
                Display(GetString(Resource.String.select_date));
                return;
            }
            if (currencyValue.Equals(GetString(Resource.String.ChooseCurrency)))
            {
                Display(GetString(Resource.String.SelectCurrency));
                return;
            }
            if (bitmap == null)
            {
                Display(GetString(Resource.String.ImageNotUploaded));
                return;
            }
            if (amountValue <= 0)
            {
                Display(GetString(Resource.String.CheckAmount));
                return;
            }
            if (amountComment.Equals(""))
            {
                Display(GetString(Resource.String.comment_blank));
                return;
            }
            if (categoryValue.Equals(GetString(Resource.String.ChooseCategory)))
            {
                Display(GetString(Resource.String.SelectCategory));
                return;
            }
            //if (bitmap != null)
            //{
            //    byte[] bitmapData;
            //    using (var stream = new MemoryStream())
            //    {

            //        bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
            //        bitmapData = stream.ToArray();
            //    //    var claimImageResponse = new EmployeeRepository().FileUpload(fileName, bitmapData);
            //    //    if (claimImageResponse.success)
            //    //    {
            //    //        Claims claim = new Claims
            //    //        {
            //    //            financeManager = financeManager,
            //    //            template = claimTemplate,
            //    //            amount = amountValue,
            //    //            attachment = claimImageResponse.data.path,
            //    //            currency = currencyValue,
            //    //            category = categoryValue,
            //    //            date = claimDateValue,
            //    //            employeeComments = amountCommentText.Text,
            //    //            status = "Open"
            //    //        };
            //    //        var response = new EmployeeRepository().SaveClaim(claim);
            //    //        if (response.success)
            //    //            Display(GetString(Resource.String.SaveClaim));
            //    //    }

            //    //}
            //}

        }


        private void ClaimDateText_Click(object sender, EventArgs e)
        {
            claimDateText.Text = "";
            CustomDatePickerFragment frag = CustomDatePickerFragment.NewInstance(delegate (DateTime time)
            {
                claimDateText.Text = time.ToShortDateString();
                claimDateValue = time.ToString("yyyy/MM/dd");

            });
            frag.Show(this.FragmentManager, "fragment");
        }

        private void CurrencySpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            currencyValue = spinner.GetItemAtPosition(e.Position).ToString();
        }

        private void CategorySpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            categoryValue = spinner.GetItemAtPosition(e.Position).ToString();
        }

        private void OpenGalleryButton_Click(object sender, EventArgs e)
        {
            StartActivityForResult(new Intent(Intent.ActionPick, MediaStore.Images.Media.InternalContentUri), 2);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 2)
            {
                if (data != null)
                {
                    Uri uri = data.Data;
                    string path = GetPathToImage(uri);
                    int height = Resources.DisplayMetrics.HeightPixels - 100;
                    int width = Resources.DisplayMetrics.WidthPixels;
                    App.bitmap = path.LoadAndResizeBitmap(width, height);
                    fileName = string.Format("myPhoto{0}.jpg", Guid.NewGuid());
                    bitmap = App.bitmap;
                }
            }
            else
            {
                Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                Uri contentUri = Uri.FromFile(App._file);
                mediaScanIntent.SetData(contentUri);
                SendBroadcast(mediaScanIntent);
                int height = Resources.DisplayMetrics.HeightPixels - 100;
                int width = Resources.DisplayMetrics.WidthPixels;
                App.bitmap = App._file.Path.LoadAndResizeBitmap(width, height);
                bitmap = App.bitmap;
                GC.Collect();
            }
        }

        private void loadPhoto(int width, int height)
        {

            AlertDialog.Builder imageDialog = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)this.GetSystemService(LayoutInflaterService);
            View layout = inflater.Inflate(Resource.Layout.CustomFullImageDialog,
                    (ViewGroup)FindViewById(Resource.Id.layout_root));
            ImageView image = (ImageView)layout.FindViewById(Resource.Id.fullImage);
            if (bitmap != null)
                image.SetImageBitmap(bitmap);
            imageDialog.SetView(layout);
            imageDialog.Create();
            imageDialog.Show();
        }

        private void CreateDirectoryForPictures()
        {
            App._dir = new Java.IO.File(
                Environment.GetExternalStoragePublicDirectory(
                    Environment.DirectoryPictures), "ClaimImages");
            if (!App._dir.Exists())
            {
                App._dir.Mkdirs();
            }
        }

        private bool IsThereAnAppToTakePictures()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities =
                PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }

        private void TakeAPicture(object sender, EventArgs eventArgs)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            App._file = new Java.IO.File(App._dir, string.Format("myPhoto{0}.jpg", Guid.NewGuid()));
            fileName = App._file.Name;
            intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(App._file));
            StartActivityForResult(intent, 0);
        }

        private string GetPathToImage(Uri uri)
        {
            string path = null;
            string[] projection = new[] { MediaStore.Images.Media.InterfaceConsts.Data };
            using (ICursor cursor = ContentResolver.Query(uri, projection, null, null, null))
            {
                if (cursor != null)
                {
                    int columnIndex = cursor.GetColumnIndexOrThrow(MediaStore.Images.Media.InterfaceConsts.Data);
                    cursor.MoveToFirst();
                    path = cursor.GetString(columnIndex);
                }
            }
            return path;
        }

        public static class App
        {
            public static Java.IO.File _file;
            public static Java.IO.File _dir;
            public static Bitmap bitmap;
        }
        async System.Threading.Tasks.Task FetchData()
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                //var settingList = new EmployeeRepository().GetSettings();
                //if (!settingList.success)
                //    return;
                //else
                //{
                //    foreach (var setting in settingList.data)
                //    {
                //        var expenseList = setting.ExpenseCategory;
                //        foreach (var expense in expenseList)
                //            expenseCategoryList.Add(expense.Name);
                //        var currencyList = setting.Currencies;
                //        foreach (var currencyTpes in currencyList)
                //            currencyTypesList.Add(currencyTpes.Symbol);
                //        financeManager = setting.FinanceManager;
                //        claimTemplate = setting.ClaimTemplate;
                //    }
                //}
            });
        }



        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this, this, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }
    }

}