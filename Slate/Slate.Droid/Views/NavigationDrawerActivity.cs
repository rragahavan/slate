using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using Slate.Droid.Adapters;
using Slate.Droid.Classes;
//using OfficeTools.Employees.Droid.Adapters;
//using OfficeTools.Employees.Droid.Classes;
using System;
using System.Collections.Generic;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "NavigationDrawerActivity")]
    public class NavigationDrawerActivity : AppCompatActivity
    {
        private DrawerLayout mDrawerLayout;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Drawer);
            AppPreferences appPreference = new AppPreferences(this);
        }

        public void Set(String[] navMenuTitles, TypedArray navMenuIcons)
        {
            mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ListView mDrawerList = FindViewById<ListView>(Resource.Id.left_drawer);
            DisplayImageOptions displayImageOption;
            View listHeaderView = LayoutInflater.Inflate(Resource.Layout.DrawerHeaderList, null, false);
            TextView profileNameText = (TextView)listHeaderView.FindViewById(Resource.Id.profileNameText);
            ImageView profileImage = (ImageView)listHeaderView.FindViewById(Resource.Id.profileImage);
            ImageView logoutIcon = (ImageView)listHeaderView.FindViewById(Resource.Id.logoutImage);
            ImageView closeDrawerIcon = (ImageView)listHeaderView.FindViewById(Resource.Id.closeDrawerImage);
            profileNameText.Text = AppPreferences.GetFisrtName() + " " + AppPreferences.GetLastName();
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            ImageLoader imageLoader = ImageLoader.Instance;
            displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
            .Build();
            imageLoader.DisplayImage(AppPreferences.GetImagePath(), profileImage,
                displayImageOption, null);
            mDrawerList.AddHeaderView(listHeaderView);
            List<NavigationDrawerItem> navDrawerItems = new List<NavigationDrawerItem>();
            for (int i = 0; i < navMenuTitles.Length; i++)
            {
                navDrawerItems.Add(new NavigationDrawerItem(navMenuTitles[i], navMenuIcons.GetResourceId(i, -1)));

            }
            NavigationListAdapter adapter = new NavigationListAdapter(Application.Context, navDrawerItems);
            mDrawerList.Adapter = adapter;
            mDrawerList.ItemClick += MDrawerList_ItemClick;
            logoutIcon.Click += LogoutIcon_Click;
            closeDrawerIcon.Click += CloseDrawerIcon_Click;
        }

        private void CloseDrawerIcon_Click(object sender, EventArgs e)
        {
            mDrawerLayout.CloseDrawers();
        }

        private void LogoutIcon_Click(object sender, EventArgs e)
        {
            AppUtil.DialogBox(this, typeof(LoginActivity), this);
        }

        private void MDrawerList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            DisplayView(e.Position);

        }

        private void DisplayView(int position)
        {
            Intent i;

            switch (position)
            {

                case 1:
                    i = new Intent(this, typeof(DashboardActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;

                case 2:
                    i = new Intent(this, typeof(HrmsActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;

                case 3:
                    i = new Intent(this, typeof(LeaveManagementActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;

                case 4:
                    i = new Intent(this, typeof(HolidayActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;

                case 5:
                    i = new Intent(this, typeof(AttendanceActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;
                case 6:
                    i = new Intent(this, typeof(QuickPageActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;

            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                mDrawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                return true;
            }
            return base.OnOptionsItemSelected(item);
        }

    }

}
