using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using System.Threading.Tasks;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/ic_launcher")]
    public class SplashScreenActivity : AppCompatActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SplashScreenLayout);

            Task startupWork = new Task(() =>
            {
                Task.Delay(200);

            });

            startupWork.ContinueWith(t =>
            {
                StartActivity(new Intent(Application.Context, typeof(LoginActivity)));
                //StartActivity(new Intent(Application.Context, typeof(LocationActivity)));
                Finish();
                OverridePendingTransition(Resource.Animation.fade_in, Resource.Animation.zoom_out);
            }, TaskScheduler.FromCurrentSynchronizationContext());

            startupWork.Start();
        }
    }
}