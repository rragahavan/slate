using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using Slate.Droid.Classes;
using Slate.Models;
using Slate.ViewModels;
using System;
using System.Collections.Generic;
using Uri = Android.Net.Uri;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "HrmsActivity")]
    public class HrmsActivity : NavigationDrawerActivity
    {
        private List<Employee> employeeDetails = new List<Employee>();
        private Button nextPageButton;
        private Button previousPageButton;
        private int pageNumber = 1;
        private int totalPages = 1;
        private EmployeeAdapter mAdapter;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.HrmsLayout);
            nextPageButton = FindViewById<Button>(Resource.Id.nextPageButton);
            previousPageButton = FindViewById<Button>(Resource.Id.previousPageButton);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Employees);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            if (AppUtil.IsNetworkAvailable(this))
            {
                ProgressDialog myProgressBar = new ProgressDialog(this);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
                if (pageNumber == 1)
                    previousPageButton.Enabled = false;
                if (pageNumber == totalPages)
                    nextPageButton.Enabled = false;

                RecyclerView mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                mRecyclerView.SetLayoutManager(mLayoutManager);
                mAdapter = new EmployeeAdapter(employeeDetails, this);
                mAdapter.ItemClick += OnItemClick;
                mRecyclerView.SetAdapter(mAdapter);

                previousPageButton.Click += async delegate
                {
                    if (pageNumber <= totalPages)
                        pageNumber = pageNumber - 1;
                    await FetchData();
                    mAdapter = new EmployeeAdapter(employeeDetails, this);
                    mRecyclerView.SetAdapter(mAdapter);
                    if (pageNumber == 1)
                        previousPageButton.Enabled = false;
                    else
                        previousPageButton.Enabled = true;
                    if (pageNumber == totalPages)
                        nextPageButton.Enabled = false;
                    else
                        nextPageButton.Enabled = true;


                };
                nextPageButton.Click += async delegate
                {
                    if (pageNumber < totalPages)
                        pageNumber = pageNumber + 1;
                    await FetchData();
                    mAdapter = new EmployeeAdapter(employeeDetails, this);
                    mRecyclerView.SetAdapter(mAdapter);

                    if (pageNumber == 1)
                        previousPageButton.Enabled = false;
                    else
                        previousPageButton.Enabled = true;
                    if (pageNumber == totalPages)
                        nextPageButton.Enabled = false;

                };
            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.NoInternetConnection), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }
        }

        void OnItemClick(object sender, int position)
        {

            Intent i = new Intent(this, typeof(SingleUserDetailsActivity));
            i.PutExtra("position", position);
            i.PutExtra("empcode", employeeDetails[position].EmpCode);
            i.PutExtra("empId", employeeDetails[position].Id);
            StartActivity(i);
        }

        public class EmployeeViewHolder : RecyclerView.ViewHolder
        {
            public ImageView ProfileImage { get; private set; }

            public TextView FirstName { get; private set; }

            public TextView Designation { get; private set; }

            public TextView EmailAddress { get; private set; }

            public TextView MobileNumber { get; private set; }

            public EmployeeViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                ProfileImage = itemView.FindViewById<ImageView>(Resource.Id.imageView);
                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                EmailAddress = itemView.FindViewById<TextView>(Resource.Id.emailAddressText);
                MobileNumber = itemView.FindViewById<TextView>(Resource.Id.mobileText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }

        public class EmployeeAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<Employee> mEmployee;
            public Context context;

            public EmployeeAdapter(List<Employee> employee, Context context)
            {
                mEmployee = employee;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerEmployeeItems, parent, false);
                EmployeeViewHolder vh = new EmployeeViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                EmployeeViewHolder vh = holder as EmployeeViewHolder;
                DisplayImageOptions displayImageOption;
                var emp = mEmployee[position];
                ImageLoader imageLoader = ImageLoader.Instance;
                displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
                .Build();
                imageLoader.DisplayImage(emp.ProfileImage, vh.ProfileImage,
                    displayImageOption, null);
                vh.FirstName.Text = emp.FirstName + " " + emp.LastName;
                vh.Designation.Text = emp.JobTitle;
                vh.EmailAddress.Text = emp.Email;
                vh.MobileNumber.Text = emp.Mobile;

                vh.EmailAddress.Click += delegate
                {
                    var email = new Intent(Intent.ActionSend);
                    email.PutExtra(Intent.ExtraEmail, new string[] { vh.EmailAddress.Text.ToString() });
                    email.SetType("message/rfc822");
                    context.StartActivity(email);

                };

                vh.MobileNumber.Click += delegate
                {
                    Intent callIntent = new Intent(Intent.ActionDial);
                    callIntent.SetData(Uri.Parse("tel:" + mEmployee[position].Mobile));
                    context.StartActivity(callIntent);

                };


            }

            public override int ItemCount
            {
                get { return mEmployee.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }

        public override void OnBackPressed()
        {
            AppUtil.DialogBox(this, typeof(LoginActivity), this);
        }

        public async System.Threading.Tasks.Task FetchData()
        {
                 EmployeeListViewModel employeeListViewModel = new EmployeeListViewModel();
                var employeeListResponse = await employeeListViewModel.EmployeeList(pageNumber, AppPreferences.GetAccessToken());
                 if (employeeListResponse!=null)
                {
                    employeeDetails =employeeListResponse.Employees;
                    pageNumber = employeeListResponse.Page;
                    totalPages = employeeListResponse.TotalPages;
                    //  mAdapter.NotifyDataSetChanged();
                }
            
        }

    }
}
