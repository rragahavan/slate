using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using OfficeTools.Employees.Droid.Adapters;
using Slate.Droid.Adapters;
using Slate.Droid.Classes;
using Slate.Models;
using Slate.Repositories;
using System;
using System.Collections.Generic;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using System.Threading.Tasks;
using Slate.ViewModels;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "DashboardActivity")]
    public class DashboardActivity : NavigationDrawerActivity
    {
        private Dashboard dashboardDetails = new Dashboard();
        RelativeLayout birthdayLinearLayout;
        RelativeLayout anniversaryLinearLayout;
        private TextView anniverssaryTitleText;
        private TextView birthdayTitleText;
        private TextView holidaysTitleText;
        private RecyclerView holidayRecyclerView;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DashboradLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Dashboard);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            birthdayTitleText = FindViewById<TextView>(Resource.Id.birthdayTitleText);
            holidaysTitleText = FindViewById<TextView>(Resource.Id.holidaysTitleText);
            anniverssaryTitleText = FindViewById<TextView>(Resource.Id.anniverssaryTitleText);
            anniversaryLinearLayout = FindViewById<RelativeLayout>(Resource.Id.anniversaryLinearLayout);
            birthdayLinearLayout = FindViewById<RelativeLayout>(Resource.Id.birthdayLinearLayout);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.viewPager);
            ViewPager anniversaryViewPager = FindViewById<ViewPager>(Resource.Id.anniversaryViewPagerLayout);
            ViewPager spotlightViewPager = FindViewById<ViewPager>(Resource.Id.spotlightViewpagerLayout);
            ViewPager newEmployeeViewPager = FindViewById<ViewPager>(Resource.Id.newEmployeeViewpager);
            FragmentPagerAdapter myPagerAdapter, anniversaryPagerAdapter, spotlightPagerAdapter, newEmployeePagerAdapter;
            if (AppUtil.IsNetworkAvailable(this))
            {
                ProgressDialog myProgressBar = new ProgressDialog(this);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
                anniverssaryTitleText.Click += delegate { ExpandableView(anniversaryLinearLayout, anniverssaryTitleText, Resource.Drawable.ic_action_empl_ann); };
                birthdayTitleText.Click += delegate { ExpandableView(birthdayLinearLayout, birthdayTitleText, Resource.Drawable.ic_action_empl_birth); };
                holidaysTitleText.Click += delegate { ExpandableView(holidayRecyclerView, holidaysTitleText, Resource.Drawable.ic_action_holiday); };
                myPagerAdapter = new BirthdayViewPagerAdapter(SupportFragmentManager, dashboardDetails.UpcomingBirthdays, this);
                viewPager.Adapter = myPagerAdapter;
                TabLayout tablayout = FindViewById<TabLayout>(Resource.Id.tabDots);
                tablayout.SetupWithViewPager(viewPager);

                spotlightPagerAdapter = new SpotlightAdapter(SupportFragmentManager, dashboardDetails.SpotlightEmployees, this);
                spotlightViewPager.Adapter = spotlightPagerAdapter;
                TabLayout spotlightTabLayout = FindViewById<TabLayout>(Resource.Id.spotlightTabDot);
                spotlightTabLayout.SetupWithViewPager(spotlightViewPager);

                newEmployeePagerAdapter = new NewEmployeeAdapter(SupportFragmentManager, dashboardDetails.NewEmployees, this);
                newEmployeeViewPager.Adapter = newEmployeePagerAdapter;
                TabLayout newEmployeeTabLayout = FindViewById<TabLayout>(Resource.Id.newEmployeeTabDot);
                newEmployeeTabLayout.SetupWithViewPager(newEmployeeViewPager);

                anniversaryPagerAdapter = new AnniversaryPagerAdapter(SupportFragmentManager, dashboardDetails.UpcomingAnniversaries, this);
                anniversaryViewPager.Adapter = anniversaryPagerAdapter;
                TabLayout anniversaryTabLayout = FindViewById<TabLayout>(Resource.Id.anniversaryTabDot);
                anniversaryTabLayout.SetupWithViewPager(anniversaryViewPager);

                holidayRecyclerView = FindViewById<RecyclerView>(Resource.Id.holidayRecyclerView);
                LinearLayoutManager holidayLayoutManager = new LinearLayoutManager(this);
               
                holidayRecyclerView.SetLayoutManager(holidayLayoutManager);
                DashboardHolidayAdapter holidayAdapter = new DashboardHolidayAdapter(dashboardDetails.UpcomingHolidays, this);
                holidayRecyclerView.SetAdapter(holidayAdapter);
                RecyclerView eventsRecyclerView = FindViewById<RecyclerView>(Resource.Id.eventsRecyclerView);
                RecyclerView.LayoutManager eventsLayoutManager = new LinearLayoutManager(this);
                eventsRecyclerView.SetLayoutManager(eventsLayoutManager);
                EventsAdapter eventsAdapter = new EventsAdapter(dashboardDetails.UpcomingEvents, this);
                eventsRecyclerView.SetAdapter(eventsAdapter);
            }
        }

        private async System.Threading.Tasks.Task FetchData()
        {
            DashboardViewModel dashboardViewModel = new DashboardViewModel();
           dashboardDetails = await dashboardViewModel.GetSettings(AppPreferences.GetAccessToken());

        }

        public void ExpandableView(View cardView, TextView downArrow, int drawableImage)
        {
            if (cardView.Visibility.ToString().Equals("Visible"))
            {
                cardView.Visibility = ViewStates.Gone;
                downArrow.SetCompoundDrawablesWithIntrinsicBounds(drawableImage, 0, Resource.Drawable.ic_action_down_arrow, 0);
            }
            else
            {
                cardView.Visibility = ViewStates.Visible;
                downArrow.SetCompoundDrawablesWithIntrinsicBounds(drawableImage, 0, Resource.Drawable.ic_action_up_arrow, 0);
            }
        }

        public class DashBoardHolidayViewHolder : RecyclerView.ViewHolder
        {
            public TextView HolidayName { get; private set; }
            public TextView HolidayDate { get; private set; }


            public DashBoardHolidayViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {
                HolidayName = itemView.FindViewById<TextView>(Resource.Id.holidayNameText);
                HolidayDate = itemView.FindViewById<TextView>(Resource.Id.holidayDateText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }



        public class DashboardHolidayAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<UpcomingHoliday> upcomingBirthday;
            public Context context;

            public DashboardHolidayAdapter(List<UpcomingHoliday> upcomingBirthday, Context context)
            {
                this.upcomingBirthday = upcomingBirthday;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerHolidayDashboardItems, parent, false);
                DashBoardHolidayViewHolder vh = new DashBoardHolidayViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                DashBoardHolidayViewHolder vh = holder as DashBoardHolidayViewHolder;
                var emp = upcomingBirthday[position];
                vh.HolidayName.Text = emp?.Name;
                vh.HolidayDate.Text = Convert.ToDateTime(emp?.Date).ToString("MMM dd, yyyy");
            }

            public override int ItemCount
            {
                get { return upcomingBirthday.Count; }
            }

            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }


        public class EventsViewHolder : RecyclerView.ViewHolder
        {
            public TextView FirstName { get; private set; }
            public TextView Designation { get; private set; }

            public EventsViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }
        }

        public class EventsAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<UpcomingEvent> upcomingBirthday;
            public Context context;

            public EventsAdapter(List<UpcomingEvent> upcomingBirthday, Context context)
            {
                this.upcomingBirthday = upcomingBirthday;
                this.context = context;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerEventsItems, parent, false);
                EventsViewHolder vh = new EventsViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                EventsViewHolder vh = holder as EventsViewHolder;
                var emp = upcomingBirthday[position];
                vh.FirstName.Text = emp.Title;
                vh.Designation.Text = Convert.ToDateTime(emp.EventDate).ToString("MMM dd, yyyy");
            }

            public override int ItemCount
            {
                get { return upcomingBirthday.Count; }
            }

            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }

        public override void OnBackPressed()
        {
            AppUtil.DialogBox(this, typeof(LoginActivity), this);
        }

    }
}
