
using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Widget;
using Slate.Droid.Adapters;
using Slate.Droid.Classes;

namespace Slate.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "TimesheetActivity")]
    public class TimesheetActivity : NavigationDrawerActivity
    {

        private readonly string[] tabTitles = { "Add Timesheet", "Timesheet Entry" };

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.TimesheetTabLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Timesheet);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.timesheetViewPager);
            FragmentPagerAdapter myPagerAdapter = new TimesheetAdapter(SupportFragmentManager, tabTitles, this);
            viewPager.Adapter = myPagerAdapter;
            myPagerAdapter.NotifyDataSetChanged();
            viewPager.SetPageTransformer(true, new ScaleTransformer());
            TabLayout tabLayout = FindViewById<TabLayout>(Resource.Id.SlidingTabs);
            tabLayout.SetupWithViewPager(viewPager);
            tabLayout.SetTabTextColors(Android.Graphics.Color.White, Android.Graphics.Color.White);
            tabLayout.TabSelected += (object sender, TabLayout.TabSelectedEventArgs e) =>
            {
                var tab = e.Tab;
                switch (tab.Position)
                {
                    case 0:
                        viewPager.SetCurrentItem(0, true);
                        toolbarTitle.Text = "Add Timesheet";

                        break;
                    case 1:
                        viewPager.SetCurrentItem(1, true);
                        toolbarTitle.Text = "Timesheet Entry";
                        break;
                    default:
                        toolbarTitle.Text = "Timesheet";
                        break;
                }

            };
        }

    }
}