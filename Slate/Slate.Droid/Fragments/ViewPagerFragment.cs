using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using Slate.Models;

using System;

namespace Slate.Droid.Fragments
{
    public class ViewPagerFragment : Fragment
    {
        public string FirstName { get; set; }
        public string ProfileImage { get; set; }
        public string JoiningDate { get; set; }
        public string BirthDate { get; set; }

        public ViewPagerFragment()
        {

        }

        public ViewPagerFragment(UpcomingBirthday upCommingBirthday)
        {
            FirstName = upCommingBirthday.FirstName;
            ProfileImage = upCommingBirthday.ProfileImage;
            BirthDate = upCommingBirthday.BirthDate;
        }
        public ViewPagerFragment(UpcomingAnniversary upCommingAnniversary)
        {
            JoiningDate = upCommingAnniversary.JoiningDate;
            FirstName = upCommingAnniversary.FirstName;
            ProfileImage = upCommingAnniversary.ProfileImage;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            View view = inflater.Inflate(Resource.Layout.RecyclerAnniversaryItems, container, false);
            TextView firstName = view.FindViewById<TextView>(Resource.Id.textView);
            ImageView ProfileImageView = view.FindViewById<ImageView>(Resource.Id.imageView);
            TextView Designation = view.FindViewById<TextView>(Resource.Id.designationText);
            DisplayImageOptions displayImageOption;
            ImageLoader imageLoader = ImageLoader.Instance;
            displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
            .Build();
            imageLoader.DisplayImage(this.ProfileImage, ProfileImageView,
                displayImageOption, null);
            firstName.Text = this.FirstName;
            if (!string.IsNullOrEmpty(BirthDate))
                Designation.Text = Convert.ToDateTime(BirthDate).ToString("MMM dd, yyyy");
            if (!string.IsNullOrEmpty(JoiningDate))
                Designation.Text = Convert.ToDateTime(JoiningDate).ToString("MMM dd, yyyy");
            return view;
        }
    }
}