using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Slate.Droid.Classes;
using Slate.Models;
using Slate.ViewModels;
using System;
using System.Collections.Generic;

namespace Slate.Droid.Fragments
{
    public class ApplyLeaveFragment : Fragment
    {
        private EditText startDateText;
        private EditText endDateText;
        private EditText commentText;
        private EditText numberOfDaysTexts;
        private DateTime startDate;
        private DateTime endDate;
        private string endDateValue;
        private string startDateValue;
        private string leaveTypeValue = "";
        private int numberOfDays;
        private Spinner leaveTypeSpinner;
        private string template;
        private List<string> leaveTypeList = new List<string>();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.ApplyLeaveLayout, container, false);
            Button applyButton = view.FindViewById<Button>(Resource.Id.applyButton);
            startDateText = view.FindViewById<EditText>(Resource.Id.startDateText);
            endDateText = view.FindViewById<EditText>(Resource.Id.endDateText);
            commentText = view.FindViewById<EditText>(Resource.Id.commentText);
            numberOfDaysTexts = view.FindViewById<EditText>(Resource.Id.numberOfDaysText);
            startDateText.Click += StartDateText_Click;
            endDateText.Click += EndDate_Click;
            applyButton.Click += ApplyButton_Click;
            leaveTypeSpinner = view.FindViewById<Spinner>(Resource.Id.typeOfLeave);
            leaveTypeList.Add(GetString(Resource.String.LeaveType));
            Fetch();
            leaveTypeSpinner.ItemSelected += LeaveTypeSpinner_ItemSelected;
            return view;
        }

        private void LeaveTypeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            leaveTypeValue = spinner.GetItemAtPosition(e.Position).ToString();
        }

        private void StartDateText_Click(object sender, EventArgs e)
        {

            startDateText.Text = "";
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                startDateText.Text = time.ToShortDateString();
                startDateValue = time.ToString("yyyy/MM/dd");
                startDate = time;
                if (endDate != DateTime.MinValue)
                {
                    numberOfDays = CalculateLeaveDays();
                    if (numberOfDays != 0)
                        numberOfDaysTexts.Text = numberOfDays.ToString();
                    else
                    {
                        Display(GetString(Resource.String.ValdateDate));
                        numberOfDaysTexts.Text = "0";
                    }
                }
            });
            frag.Show(this.Activity.FragmentManager, "fragment");
        }

        private void EndDate_Click(object sender, EventArgs e)
        {
            endDateText.Text = "";
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                endDateText.Text = time.ToShortDateString();
                endDateValue = time.ToString("yyyy/MM/dd");
                endDate = time;
                if (startDate != DateTime.MinValue)
                {
                    numberOfDays = CalculateLeaveDays();
                    if (numberOfDays != 0)
                        numberOfDaysTexts.Text = numberOfDays.ToString();
                    else
                    {
                        Display(GetString(Resource.String.ValdateDate));
                        numberOfDaysTexts.Text = "0";
                    }

                }
            });
            frag.Show(this.Activity.FragmentManager, "fragment");

        }


        private void ApplyButton_Click(object sender, EventArgs e)
        {
            string comments = commentText.Text;
            if (startDate == DateTime.MinValue || endDate == DateTime.MinValue)
            {
                Display(GetString(Resource.String.select_date));
                return;
            }

            if (leaveTypeValue.Equals(GetString(Resource.String.LeaveType)))
            {
                Display(GetString(Resource.String.select_leave_type));
                return;
            }
            if (numberOfDays <= 0)
            {
                Display(GetString(Resource.String.check_to_date));
                return;
            }
            if (comments.Equals(""))
            {
                Display(GetString(Resource.String.comment_blank));
                return;
            }
            var leaveData = new LeaveRequest { template = template, fromDate = startDateValue, leaveType = leaveTypeValue, numDays = numberOfDays, toDate = endDateValue, UserComments = comments };
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //var response = new EmployeeRepository().AddLeaveDetails(leaveData);
                //if (response.Success)
                //{
                //    Display(GetString(Resource.String.LeaveAdded));
                //    startDate = DateTime.MinValue;
                //    endDate = DateTime.MinValue;
                //    startDateText.Text = "";
                //    endDateText.Text = "";
                //    numberOfDaysTexts.Text = "";
                //    commentText.Text = "";

                //}
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
        }

        public int CalculateLeaveDays()
        {

            DateTime firstDay = startDate;
            DateTime lastDay = endDate;
            List<DateTime> bankHolidays = new List<DateTime>();
            bankHolidays.Add(new DateTime(2017, 01, 26, 0, 0, 0, 0));

            TimeSpan span = lastDay - firstDay;
            int businessDays = span.Days + 1;
            int fullWeekCount = businessDays / 7;

            if (businessDays > fullWeekCount * 7)
            {
                int firstDayOfWeek = (int)firstDay.DayOfWeek;
                int lastDayOfWeek = (int)lastDay.DayOfWeek;
                if (lastDayOfWeek < firstDayOfWeek)
                    lastDayOfWeek += 7;
                if (firstDayOfWeek <= 6)
                {
                    if (lastDayOfWeek >= 7)
                        businessDays -= 2;
                    else if (lastDayOfWeek >= 6)
                        businessDays -= 1;
                }
                else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)
                    businessDays -= 1;
            }
            businessDays -= fullWeekCount + fullWeekCount;
            foreach (DateTime bankHoliday in bankHolidays)
            {
                DateTime bh = bankHoliday.Date;
                if (firstDay <= bh && bh <= lastDay)
                    --businessDays;
            }
            Console.WriteLine(businessDays);
            return businessDays > 0 ? businessDays : 0;
        }

        public async void Fetch()
        {
            await FetchData();
            ArrayAdapter<String> leaveTypeArrayAdapter = new ArrayAdapter<String>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, leaveTypeList);
            leaveTypeArrayAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            leaveTypeSpinner.Adapter = leaveTypeArrayAdapter;
        }

        async System.Threading.Tasks.Task FetchData()
        {
                SettingsViewModel settingsViewModel = new SettingsViewModel();
                var settingList = await settingsViewModel.GetSettings(AppPreferences.GetAccessToken());
                if (settingList==null)
                    return;
                else
                {

                 template=settingList?.MailTemplate;
                 foreach (var leaveType in settingList.LeaveTypes)
                        leaveTypeList.Add(leaveType.Description);
                }
        }
        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }
    }
}