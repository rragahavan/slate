using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Slate.Droid.Classes;
//using RadialProgress;

namespace Slate.Droid.Fragments
{
    public class LeaveSummaryFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.LeaveSummaryLayout, container, false);
            TextView leaveTypetText = (view).FindViewById<TextView>(Resource.Id.leaveTypeText);
            TextView totalText = (view).FindViewById<TextView>(Resource.Id.totalText);
            TextView appliedText = (view).FindViewById<TextView>(Resource.Id.appliedText);
            TextView balancetext = (view).FindViewById<TextView>(Resource.Id.balanceText);
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //var response = new Repositories.EmployeeRepository().GetLeaveDetails();
                //if (response.Success)
                //{
                //    leaveTypetText.Text = response.Data.leaveSummary.leavesummary[0].leaveType.Description;
                //    totalText.Text = "Total Leaves: " + response.Data.leaveSummary.leavesummary[0].total.ToString();
                //    appliedText.Text = (response.Data.leaveSummary.leavesummary[0].total - response.Data.leaveSummary.leavesummary[0].balance).ToString();
                //    balancetext.Text = response.Data.leaveSummary.leavesummary[0].balance.ToString();

                //}
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
            return view;

        }
        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();

        }
    }
}