using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Slate.Droid.Classes;
using Slate.Models;
using Slate.ViewModels;
//using OfficeTools.Shared.Models;
using System;
using System.Collections.Generic;

namespace Slate.Droid.Fragments
{
    public class MyLeaveFragment : Fragment
    {

        private List<LeaveRequest> leaveDetails = new List<LeaveRequest>();
        LeaveAdapter mAdapter;
        private Context context;

        public override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           

        }

        public override void OnResume()
        {
            base.OnResume();
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //leaveDetails = new Repositories.EmployeeRepository().GetLeaveDetails().Data.LeaveRequests;
                //mAdapter.NotifyDataSetChanged();
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.MyLeaveLayout, container, false);
            context = this.Activity;
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {

                                //leaveDetails = new Repositories.EmployeeRepository().GetLeaveDetails().Data.LeaveRequests;
                RecyclerView mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.Activity);
                mRecyclerView.SetLayoutManager(mLayoutManager);
                mAdapter = new LeaveAdapter(leaveDetails, context);
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
            return view;
        }


        public class LeaveViewHolder : RecyclerView.ViewHolder
        {

            public TextView StartDate { get; private set; }
            public TextView EndDate { get; private set; }
            public TextView LeaveType { get; private set; }
            public TextView Days { get; private set; }
            public TextView Status { get; private set; }
            public LinearLayout StatusLeaveLayout { get; private set; }
            public LinearLayout LinearOneLayout { get; private set; }
            public LinearLayout LinearTwoLayout { get; private set; }
            public LinearLayout LinearThreeLayout { get; private set; }
            public TextView Days1 { get; private set; }
            public TextView Status1 { get; private set; }

            public LeaveViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                StartDate = itemView.FindViewById<TextView>(Resource.Id.startDateText);
                EndDate = itemView.FindViewById<TextView>(Resource.Id.endDateText);
                LeaveType = itemView.FindViewById<TextView>(Resource.Id.leaveTypeText);
                Days = itemView.FindViewById<TextView>(Resource.Id.daysText);
                Status = itemView.FindViewById<TextView>(Resource.Id.statusText);
                Days1 = itemView.FindViewById<TextView>(Resource.Id.daysText1);
                Status1 = itemView.FindViewById<TextView>(Resource.Id.statusText1);
                LinearOneLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearOne);
                LinearTwoLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearFour);
            }
        }

        public class LeaveAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<LeaveRequest> mLeave;
            public Context context;

            public LeaveAdapter(List<LeaveRequest> leave, Context context)
            {
                mLeave = leave;
                this.context = context;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.MyLeaveItemsRecycler, parent, false);
                LeaveViewHolder vh = new LeaveViewHolder(itemView, OnClick);
                return vh;
            }
            public override int GetItemViewType(int position)
            {
                return position;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                LeaveViewHolder vh = holder as LeaveViewHolder;
                if (position % 2 != 0)
                {
                    vh.LinearOneLayout.Visibility = ViewStates.Gone;
                    vh.LinearTwoLayout.Visibility = ViewStates.Visible;
                }
                var leave = mLeave[position];
                vh.EndDate.Text = Convert.ToDateTime(leave.toDate).ToString("MMM dd, yyyy");
                vh.StartDate.Text = Convert.ToDateTime(leave.fromDate).ToString("MMM dd, yyyy");
                vh.Days.Text = leave?.numDays.ToString();
                vh.Days1.Text = leave?.numDays.ToString();
                vh.LeaveType.Text = leave?.leaveType;
                var s = leave?.status;
                if (s.Equals("Approved"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                }
                else if (s.Equals("Open"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.LinearOneLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.LightOrange)));
                    vh.LinearTwoLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.LightOrange)));
                }
                else
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.LinearOneLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.DarkRed)));
                    vh.LinearTwoLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.DarkRed)));
                }
                vh.LinearOneLayout.Click += delegate
                {
                    var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                    var alert = builder.Create();
                    View itemView1 = LayoutInflater.From(context).
                            Inflate(Resource.Layout.LeaveCommentDialog, null, false);
                    alert.SetView(itemView1);
                    alert.Show();
                    TextView commentText = (itemView1).FindViewById<TextView>(Resource.Id.commentText);
                    Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                    TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);
                    commentText.Text = leave?.managerComments;
                    closeButton.Click += delegate { alert.Cancel(); };
                    acceptButton.Click += delegate { alert.Cancel(); };
                };
                vh.LinearTwoLayout.Click += delegate
                {
                    var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                    var alert = builder.Create();
                    View itemView1 = LayoutInflater.From(context).
                            Inflate(Resource.Layout.LeaveCommentDialog, null, false);
                    alert.SetView(itemView1);
                    alert.Show();
                    TextView commentText = (itemView1).FindViewById<TextView>(Resource.Id.commentText);
                    Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                    TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);
                    commentText.Text = leave?.managerComments;
                    closeButton.Click += delegate { alert.Cancel(); };
                    acceptButton.Click += delegate { alert.Cancel(); };
                };

            }



            public override int ItemCount
            {
                get { return mLeave.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }


        public async System.Threading.Tasks.Task FetchData()
        {
            MyLeaveViewModel myLeaveViewModel = new MyLeaveViewModel();
            var leaveResponse = await myLeaveViewModel.GetLeaveDetails(AppPreferences.GetAccessToken());
            if (leaveResponse != null)
                leaveDetails = leaveResponse?.LeaveRequests;
            int k = 0;
        }



        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }

        public override async void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                Android.App.ProgressDialog myProgressBar = new Android.App.ProgressDialog(this.Activity);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
            }
        }
    }
}
