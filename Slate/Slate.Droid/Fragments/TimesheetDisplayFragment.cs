using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Slate.Droid.Classes;
using Slate.Models;
using Slate.Repositories;
using Slate.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Slate.Droid.Fragments
{
    public class TimesheetDisplayFragment : Fragment
    {
        private Context context;
        List<TimesheetDateEntry> timesheetDateEntryList;
        List<TimesheetEntry> timesheetEntryList;
        private Button previousButton;
        private Button nextButton;
        private Button submitButton;
        private DateTime firstDay;
        private DateTime lastDay;
        private TimesheetAdapter timesheetAdapter;
        private RecyclerView timesheetDateRecylerview;
        private RecyclerView timesheetRecylcerview;
        private TimesheetDateAdapter timesheetDateAdapter;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.TimesheetDisplayLayout, container, false);
            context = this.Activity;
            previousButton = view.FindViewById<Button>(Resource.Id.previousButton);
            nextButton = view.FindViewById<Button>(Resource.Id.nextButton);

            previousButton.Click += delegate
            {
                lastDay = firstDay.AddDays(-1);
                firstDay = firstDay.AddDays(-7);
                FetchTimesheetData();
            };
            nextButton.Click += delegate
            {
                firstDay = lastDay.AddDays(1);
                lastDay = firstDay.AddDays(6);
                FetchTimesheetData();
            };



            timesheetDateEntryList = new List<TimesheetDateEntry>();
            DateTime input = DateTime.Now;

            firstDay = input.AddDays(DayOfWeek.Sunday - input.DayOfWeek);
            lastDay = input.AddDays(DayOfWeek.Saturday - input.DayOfWeek);
            if (lastDay >= DateTime.Now)
            {
                nextButton.Enabled = false;
                nextButton.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.gray)));
            }
            else
            {
                nextButton.Enabled = true;
                nextButton.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.ButtonTextColor)));
            }
            for (var day = firstDay.Date; day.Date <= lastDay.Date; day = day.AddDays(1))
            {
                TimesheetDateEntry timesheetDateEntry = new TimesheetDateEntry();
                timesheetDateEntry.DayName = day.ToString("ddd");
                timesheetDateEntry.DayNumber = day.Day.ToString();
                timesheetDateEntry.Month = day.ToString("MMM");
                timesheetDateEntry.Date = day;
                timesheetDateEntryList.Add(timesheetDateEntry);
            }

            timesheetDateRecylerview = view.FindViewById<RecyclerView>(Resource.Id.timesheetDateRecyclerView);
            RecyclerView.LayoutManager dateLayoutManager = new LinearLayoutManager(this.Activity, LinearLayoutManager.Horizontal, false);
            //RecyclerView.LayoutManager dateLayoutManager = new LinearLayoutManager(this.Activity,LinearLayoutManager.Vertical,false);
            timesheetDateRecylerview.SetLayoutManager(dateLayoutManager);
            timesheetDateAdapter = new TimesheetDateAdapter(this.Activity, timesheetDateEntryList, context);
            timesheetDateRecylerview.SetAdapter(timesheetDateAdapter);
            timesheetDateAdapter.ItemClick += OnItemClick;

            timesheetRecylcerview = view.FindViewById<RecyclerView>(Resource.Id.timesheetRecyclerView);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.Activity);
            timesheetRecylcerview.SetLayoutManager(mLayoutManager);
            string startDate = firstDay.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
            string endDate = lastDay.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
            submitButton = view.FindViewById<Button>(Resource.Id.submitButton);

            submitButton.Click += delegate
            {
                if (AppUtil.IsNetworkAvailable(this.Activity))
                {
                    int totalHours = 0;

                    //timesheetEntryList = new EmployeeRepository().GetTimesheetEntry(startDate, endDate).Data;
                    //foreach (var list in timesheetEntryList)
                    //{
                    //    int hour = 0;
                    //    var isConverted = Int32.TryParse(list.Hours, out hour);
                    //    if (isConverted)
                    //        totalHours += hour;
                    //}
                    //TimesheetRequest timesheetRequest = new TimesheetRequest { fromDate = startDate, toDate = endDate, totalHours = totalHours };
                    //new EmployeeRepository().SaveTimesheet(timesheetRequest);

                }
            };


            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //timesheetEntryList = new EmployeeRepository().GetTimesheetEntry(startDate, endDate).Data;
                //if (timesheetEntryList.Count != 0)
                //    timesheetAdapter = new TimesheetAdapter(this.Activity, timesheetEntryList, context);
                //timesheetRecylcerview.SetAdapter(timesheetAdapter);
            }
            return view;
        }


        public class TimesheetViewHolder : RecyclerView.ViewHolder
        {

            public TextView ProjectName { get; private set; }
            public TextView TaskName { get; private set; }
            public TextView Hours { get; private set; }
            public LinearLayout LinearLayoutBackGround { get; set; }


            public TimesheetViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {
                ProjectName = itemView.FindViewById<TextView>(Resource.Id.projectNameText);
                TaskName = itemView.FindViewById<TextView>(Resource.Id.taskNameText);
                Hours = itemView.FindViewById<TextView>(Resource.Id.hoursText);
                LinearLayoutBackGround = itemView.FindViewById<LinearLayout>(Resource.Id.linearBackground1);
            }
        }

        public class TimesheetAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<TimesheetEntry> timesheetEntry;
            public Context context;
            public FragmentActivity activity;

            public TimesheetAdapter(FragmentActivity activity, List<TimesheetEntry> timesheetEntry, Context context)
            {
                this.timesheetEntry = timesheetEntry;
                this.context = context;
                this.activity = activity;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerTimesheetItems, parent, false);
                TimesheetViewHolder vh = new TimesheetViewHolder(itemView, OnClick);
                return vh;
            }
            public override int GetItemViewType(int position)
            {
                return position;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                TimesheetViewHolder vh = holder as TimesheetViewHolder;
                var timesheetEntryItem = timesheetEntry[position];
                vh.ProjectName.Text = timesheetEntryItem.ProjectName;
                vh.TaskName.Text = timesheetEntryItem.Task;
                vh.Hours.Text = timesheetEntryItem.Hours;
                if (position % 2 == 0)
                    vh.LinearLayoutBackGround.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.ShadowLightBlue)));
                else
                    vh.LinearLayoutBackGround.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.white)));
            }

            public override int ItemCount
            {
                get { return timesheetEntry.Count; }
            }

            void OnClick(int position)
            {

                ItemClick?.Invoke(this, position);


            }

        }


        void OnItemClick(object sender, int position)
        {
            var k = timesheetDateEntryList[position].Date;
            string startDate = firstDay.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
            string endDate = lastDay.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //timesheetEntryList = new EmployeeRepository().GetTimesheetEntry(startDate, endDate).Data;
            }

            var duplicateTimesheet = timesheetEntryList;
            duplicateTimesheet = duplicateTimesheet.Where(i => (Convert.ToDateTime(i.TaskDate).Date) == k.Date).ToList();
            timesheetEntryList = duplicateTimesheet;
            timesheetAdapter = new TimesheetAdapter(this.Activity, timesheetEntryList, context);
            timesheetRecylcerview.SetAdapter(timesheetAdapter);
            timesheetAdapter.NotifyDataSetChanged();
        }

        public class TimesheetDateViewHolder : RecyclerView.ViewHolder
        {

            public TextView DayName { get; private set; }
            public TextView DayNumber { get; private set; }
            public TextView MonthName { get; private set; }
            public LinearLayout LinearLayoutBackground { get; set; }

            public TimesheetDateViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {
                DayName = itemView.FindViewById<TextView>(Resource.Id.dayNameText);
                DayNumber = itemView.FindViewById<TextView>(Resource.Id.dayNumberText);
                MonthName = itemView.FindViewById<TextView>(Resource.Id.monthNameText);
                LinearLayoutBackground = itemView.FindViewById<LinearLayout>(Resource.Id.linearBackground);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }
        }

        public class TimesheetDateAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<TimesheetDateEntry> timesheetDateEntry;

            public Context context;
            public FragmentActivity activity;

            public TimesheetDateAdapter(FragmentActivity activity, List<TimesheetDateEntry> timesheetDateEntry, Context context)
            {
                this.timesheetDateEntry = timesheetDateEntry;

                this.context = context;
                this.activity = activity;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerTimesheetDateLayout, parent, false);

                TimesheetDateViewHolder vh = new TimesheetDateViewHolder(itemView, OnClick);
                return vh;
            }
            public override int GetItemViewType(int position)
            {
                return position;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                TimesheetDateViewHolder vh = holder as TimesheetDateViewHolder;
                var date = timesheetDateEntry[position];
                vh.DayName.Text = date.DayName;
                vh.DayNumber.Text = date.DayNumber;
                vh.MonthName.Text = date.Month;
                vh.LinearLayoutBackground.Click += delegate { vh.LinearLayoutBackground.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.ShadowLightBlue))); };

            }

            public override int ItemCount
            {
                get { return timesheetDateEntry.Count; }
            }

            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);

            }

        }

        public void FetchTimesheetData()
        {

            if (lastDay >= DateTime.Now)
            {
                nextButton.Enabled = false;
                nextButton.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.gray)));
            }
            else
            {
                nextButton.Enabled = true;
                nextButton.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.ButtonTextColor)));
            }

            string startDate = firstDay.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
            string endDate = lastDay.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
            timesheetDateEntryList.Clear();
            timesheetEntryList.Clear();

            for (var day = firstDay.Date; day.Date <= lastDay.Date; day = day.AddDays(1))
            {
                TimesheetDateEntry timesheetDateEntry = new TimesheetDateEntry();
                timesheetDateEntry.DayName = day.ToString("ddd");
                timesheetDateEntry.DayNumber = day.Day.ToString();
                timesheetDateEntry.Month = day.ToString("MMM");
                timesheetDateEntry.Date = day;
                timesheetDateEntryList.Add(timesheetDateEntry);
            }
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //timesheetEntryList = new EmployeeRepository().GetTimesheetEntry(startDate, endDate).Data;
                //timesheetAdapter = new TimesheetAdapter(this.Activity, timesheetEntryList, context);
                //timesheetRecylcerview.SetAdapter(timesheetAdapter);
            }
            timesheetDateAdapter.NotifyDataSetChanged();
        }

    }
}