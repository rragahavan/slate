using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using Slate.Models;
//using OfficeTools.Shared.Models;

namespace Slate.Droid.Fragments
{
    public class NewEmployeeFragment : Fragment
    {
        public string FirstName { get; set; }
        public string ProfileImage { get; set; }
        public string EmployeeEmail { get; set; }

        public NewEmployeeFragment(NewEmployee newEmployee)
        {
            FirstName = newEmployee.FirstName;
            ProfileImage = newEmployee.ProfileImage;
            EmployeeEmail = newEmployee.Email;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.RecyclerNewEmployeeLayout, container, false);
            TextView firstName = view.FindViewById<TextView>(Resource.Id.newEmployeeNameText);
            ImageView ProfileImageView = view.FindViewById<ImageView>(Resource.Id.newEmployeeImage);
            TextView Email = view.FindViewById<TextView>(Resource.Id.newEmployeeEmailText);
            DisplayImageOptions displayImageOption;
            ImageLoader imageLoader = ImageLoader.Instance;
            displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
            .Build();
            imageLoader.DisplayImage(this.ProfileImage, ProfileImageView,
                displayImageOption, null);
            firstName.Text = this.FirstName;
            if (!string.IsNullOrEmpty(EmployeeEmail))
                Email.Text = EmployeeEmail;
            return view;
        }
    }
}