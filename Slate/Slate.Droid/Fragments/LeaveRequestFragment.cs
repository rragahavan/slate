using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Slate.Droid.Classes;
using Slate.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Slate.Droid.Fragments
{
    public class LeaveRequestFragment : Fragment
    {
        private List<ManagerleaveRequest> leaveDetails = new List<ManagerleaveRequest>();
        private List<ManagerleaveRequest> openLeaveStatus = new List<ManagerleaveRequest>();
        LeavesRequestAdapter mAdapter;
        private Context context;
        private CheckBox selectAllCheckbox;
        private RecyclerView mRecyclerView;
        private string leaveActionType;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

        }

        public override void OnResume()
        {
            base.OnResume();
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //leaveDetails = new Repositories.EmployeeRepository().GetLeaveDetails().Data.ManagerleaveRequests;
                //openLeaveStatus = leaveDetails;
                //openLeaveStatus = openLeaveStatus.Where(i => i?.status == "Open").ToList();
                //mAdapter.NotifyDataSetChanged();
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.MyLeaveRequest, container, false);
            context = this.Activity;
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                //leaveDetails = new Repositories.EmployeeRepository().GetLeaveDetails().Data.ManagerleaveRequests;
                //mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
                //selectAllCheckbox = view.FindViewById<CheckBox>(Resource.Id.selectCheckbox);
                //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.Activity);
                //mRecyclerView.SetLayoutManager(mLayoutManager);
                //openLeaveStatus = leaveDetails;
                //openLeaveStatus = openLeaveStatus.Where(i => i?.status == "Open").ToList();
                //mAdapter = new LeavesRequestAdapter(this.Activity, openLeaveStatus, context, "");
                //mRecyclerView.SetAdapter(mAdapter);
                //mAdapter.NotifyDataSetChanged();


                Spinner leaveStatusSpinner = view.FindViewById<Spinner>(Resource.Id.statusOfLeave);
                leaveStatusSpinner.ItemSelected += LeaveTypeSpinner_ItemSelected;
                var adapter = ArrayAdapter.CreateFromResource(
                    this.Activity, Resource.Array.leave_status, Resource.Layout.Spinner_item);
                adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                leaveStatusSpinner.Adapter = adapter;

                Spinner leaveActionSpinner = view.FindViewById<Spinner>(Resource.Id.leaveType);
                leaveActionSpinner.ItemSelected += LeaveActionSpinner_ItemSelected;

                selectAllCheckbox.Click += (o, e) =>
                {
                    if (selectAllCheckbox.Checked.Equals(true) && !leaveActionType.Equals("Action"))
                    {
                        var leaveDetailStatus = leaveDetails;
                        leaveDetailStatus = leaveDetailStatus.Where(i => i?.status == "Open").ToList();
                        mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetailStatus, context, "Open");
                        mRecyclerView.SetAdapter(mAdapter);
                        mAdapter.NotifyDataSetChanged();
                        var idList = leaveDetailStatus.Select(i => i._id).ToList();
                        var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                        var alert = builder.Create();
                        View itemView1 = LayoutInflater.From(context).
                                Inflate(Resource.Layout.custom_dialog, null, false);
                        alert.SetView(itemView1);
                        alert.Show();
                        EditText commentText = (itemView1).FindViewById<EditText>(Resource.Id.commentText);
                        Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                        TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);
                        closeButton.Click += delegate { alert.Cancel(); };
                        acceptButton.Click += delegate
                        {
                            alert.Cancel();

                            var leaveData = new { leaveIdList = idList, managerComments = commentText.Text.ToString(), status = leaveActionType };

                            if (AppUtil.IsNetworkAvailable(context))
                            {
                                //var response = new Repositories.EmployeeRepository().UpadteAllLeavestatus(leaveData);
                                //if (response.Success)
                                //{
                                //    Toast.MakeText(this.Activity, "Leaves applied successfully", ToastLength.Long).Show();
                                //}
                            }
                        };

                    }
                };
                var leaveActionAdapter = ArrayAdapter.CreateFromResource(
                    this.Activity, Resource.Array.action_type, Resource.Layout.Spinner_item);
                adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                leaveActionSpinner.Adapter = leaveActionAdapter;
                selectAllCheckbox = view.FindViewById<CheckBox>(Resource.Id.selectCheckbox);

            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
            return view;
        }


        private void LeaveActionSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            leaveActionType = spinner.GetItemAtPosition(e.Position).ToString();
            if (!leaveActionType.Equals("Action") && selectAllCheckbox.Checked.Equals(true))
            {
                var leaveDetailStatus = leaveDetails;
                leaveDetailStatus = leaveDetailStatus.Where(i => i?.status == "Open").ToList();
                mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetailStatus, context, "Open");
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();
                var idList = leaveDetailStatus.Select(i => i._id).ToList();
                var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                var alert = builder.Create();
                View itemView1 = LayoutInflater.From(context).
                        Inflate(Resource.Layout.custom_dialog, null, false);
                alert.SetView(itemView1);
                alert.Show();
                EditText commentText = (itemView1).FindViewById<EditText>(Resource.Id.commentText);
                Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);
                closeButton.Click += delegate { alert.Cancel(); };
                acceptButton.Click += delegate
                {
                    alert.Cancel();
                    var leaveData = new { leaveIdList = idList, managerComments = commentText.Text.ToString(), status = leaveActionType };
                    if (AppUtil.IsNetworkAvailable(context))
                    {
                        //var response = new Repositories.EmployeeRepository().UpadteAllLeavestatus(leaveData);
                        //if (response.Success)
                        //{
                        //    Toast.MakeText(this.Activity, "Leaves applied successfully", ToastLength.Long).Show();
                        //}
                    }
                };
            }
        }

        private void LeaveTypeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            var leaveTypeValue = spinner.GetItemAtPosition(e.Position).ToString();
            if (!leaveTypeValue.Equals("All"))
            {
                var leaveDetailStatus = leaveDetails;
                leaveDetailStatus = leaveDetailStatus.Where(i => i?.status == leaveTypeValue).ToList();
                mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetailStatus, context, leaveTypeValue);
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();

            }
            else
            {
                mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetails, context, "");
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();

            }
        }





        public class LeaveViewHolder : RecyclerView.ViewHolder
        {

            public TextView StartDate { get; private set; }
            public TextView EndDate { get; private set; }
            public TextView LeaveType { get; private set; }
            public TextView Days { get; private set; }
            public TextView Status { get; private set; }
            public LinearLayout StatusLeaveLayout { get; private set; }
            public LinearLayout LinearOneLayout { get; private set; }
            public LinearLayout LinearTwoLayout { get; private set; }
            public LinearLayout LinearThreeLayout { get; private set; }
            public LinearLayout LeaveStatusLayout { get; private set; }
            public TextView Days1 { get; private set; }
            public TextView Status1 { get; private set; }
            public TextView ApproveText { get; private set; }
            public TextView CancelText { get; private set; }
            public CardView ParentLayout { get; private set; }
            public TextView EmployeeName { get; private set; }


            public LeaveViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {
                StartDate = itemView.FindViewById<TextView>(Resource.Id.startDateText);
                EndDate = itemView.FindViewById<TextView>(Resource.Id.endDateText);
                LeaveType = itemView.FindViewById<TextView>(Resource.Id.leaveTypeText);
                Days = itemView.FindViewById<TextView>(Resource.Id.daysText);
                Status = itemView.FindViewById<TextView>(Resource.Id.statusText);
                Days1 = itemView.FindViewById<TextView>(Resource.Id.daysText1);
                Status1 = itemView.FindViewById<TextView>(Resource.Id.statusText1);
                ApproveText = (itemView).FindViewById<TextView>(Resource.Id.approveText);
                CancelText = (itemView).FindViewById<TextView>(Resource.Id.cancelText);
                LinearOneLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearOne);
                LinearTwoLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearFour);
                LeaveStatusLayout = itemView.FindViewById<LinearLayout>(Resource.Id.statusLeaveLayout);
                ParentLayout = itemView.FindViewById<CardView>(Resource.Id.parentCardView);
                EmployeeName = itemView.FindViewById<TextView>(Resource.Id.employeeNameText);
            }
        }

        public class LeavesRequestAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<ManagerleaveRequest> mLeave;
            public Context context;
            public object filterOption;
            public FragmentActivity activity;

            public LeavesRequestAdapter(FragmentActivity activity, List<ManagerleaveRequest> leave, Context context, object filterOption = null)
            {
                mLeave = leave;
                this.context = context;
                this.filterOption = filterOption;
                this.activity = activity;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.MyLeaveRequestItemsRecycler, parent, false);
                LeaveViewHolder vh = new LeaveViewHolder(itemView, OnClick);
                return vh;
            }
            public override int GetItemViewType(int position)
            {
                return position;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                LeaveViewHolder vh = holder as LeaveViewHolder;
                if (position % 2 != 0)
                {
                    vh.LinearOneLayout.Visibility = ViewStates.Gone;
                    vh.LinearTwoLayout.Visibility = ViewStates.Visible;
                }
                var leave = mLeave[position];
                if ((!filterOption.Equals("")) && (!filterOption.Equals(leave?.status)))
                    vh.ParentLayout.Visibility = ViewStates.Gone;
                vh.EndDate.Text = Convert.ToDateTime(leave.toDate).ToString("MMM dd, yyyy");
                vh.StartDate.Text = Convert.ToDateTime(leave.fromDate).ToString("MMM dd, yyyy");
                vh.Days.Text = leave?.numDays.ToString();
                vh.Days1.Text = leave?.numDays.ToString();
                vh.EmployeeName.Text = leave.User?.FirstName;
                vh.LeaveType.Text = leave?.leaveType;
                vh.ApproveText.Click += delegate
                {
                    var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                    var alert = builder.Create();
                    View itemView1 = LayoutInflater.From(context).
                            Inflate(Resource.Layout.custom_dialog, null, false);
                    alert.SetView(itemView1);
                    alert.Show();
                    EditText commentText = (itemView1).FindViewById<EditText>(Resource.Id.commentText);
                    Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                    TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);

                    closeButton.Click += delegate { alert.Cancel(); };

                    acceptButton.Click += delegate
                    {
                        alert.Cancel();

                        List<string> idList = new List<string>();
                        idList.Add(mLeave[position]._id);

                        var leaveData = new { leaveIdList = idList, managerComments = commentText.Text.ToString(), status = "Approved" };
                        if (AppUtil.IsNetworkAvailable(context))
                        {
                            //var response = new Repositories.EmployeeRepository().UpadteLeavestatus(leaveData);
                            //if (response.Success)
                            //    DisplayToastMeassage(context.GetString(Resource.String.LeaveStatusUpdated));
                        }
                        else
                            DisplayToastMeassage(context.GetString(Resource.String.NoInternetConnection));
                    };
                };

                vh.CancelText.Click += delegate
                {
                    var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                    var alert = builder.Create();
                    View itemView1 = LayoutInflater.From(context).
                            Inflate(Resource.Layout.custom_dialog, null, false);
                    alert.SetView(itemView1);
                    alert.Show();
                    EditText commentText = (itemView1).FindViewById<EditText>(Resource.Id.commentText);
                    Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                    TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);
                    closeButton.Click += delegate
                    {
                        alert.Cancel();
                    };
                    acceptButton.Click += delegate
                    {
                        alert.Cancel();
                        if (AppUtil.IsNetworkAvailable(context))
                        {

                            List<string> idList = new List<string>();
                            idList.Add(mLeave[position]._id);
                            var leaveData = new { leaveIdList = idList, managerComments = commentText.Text.ToString(), status = "Rejected" };
                            //var response = new Repositories.EmployeeRepository().UpadteLeavestatus(leaveData);
                            //if (response.Success)
                            //    DisplayToastMeassage(context.GetString(Resource.String.LeaveStatusUpdated));
                        }
                        else
                            DisplayToastMeassage(context.GetString(Resource.String.NoInternetConnection));
                    };
                };

                var s = leave?.status;
                if (s.Equals("Approved"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                }
                else if (s.Equals("Open"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.LeaveStatusLayout.Visibility = ViewStates.Visible;
                    vh.LinearOneLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.LightOrange)));
                    vh.LinearTwoLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.LightOrange)));

                }
                else
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.LinearOneLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.DarkRed)));
                    vh.LinearTwoLayout.SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.DarkRed)));

                }
            }

            public override int ItemCount
            {
                get { return mLeave.Count; }
            }

            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);

            }

            public void DisplayToastMeassage(string toastMeassage)
            {
                CustomToast customMessage = new CustomToast(activity, activity, toastMeassage, true);
                customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
                customMessage.Show();
            }

        }
        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }
    }
}