using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;

namespace Slate.Droid.Classes
{
    class ScaleTransformer : Java.Lang.Object, ViewPager.IPageTransformer
    {


        private readonly float MinScale = 0.5f;

        public void TransformPage(View view, float position)
        {
            if (position < -1 || position > 1)
            {
                view.Alpha = 0;
            }
            else
            {
                view.Alpha = 1;
                float scale = 1 - Math.Abs(position) * (1 - MinScale);
                view.ScaleX = scale;
                view.ScaleY = scale;
                float xMargin = view.Width * (1 - scale) / 2;

                if (position < 0)
                {
                    view.TranslationX = xMargin / 2;
                }
                else
                {
                    view.TranslationX = -xMargin / 2;
                }

            }
        }
    }
}
