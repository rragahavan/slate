using Android.Content;
using Android.Preferences;

namespace Slate.Droid.Classes
{
    public class AppPreferences
    {
        private static ISharedPreferences mSharedPrefs;
        private static ISharedPreferencesEditor mPrefsEditor;
        private readonly Context mContext;
        private static string PREFERENCE_FIRST_NAME = "PREFERENCE_FIRST_NAME";
        private static string PREFERENCE_LAST_NAME = "PREFERENCE_LAST_NAME";
        private static string PREFERENCE_IMAGE_PATH = "PREFERENCE_IMAGE_PATH";
        private static string PREFERENCE_ACCESS_TOKEN = "PREFERENCE_ACCESS_TOKEN";

        public AppPreferences(Context context)
        {
            mContext = context;
            mSharedPrefs = PreferenceManager.GetDefaultSharedPreferences(mContext);
            mPrefsEditor = mSharedPrefs.Edit();
        }

        public static string GetFisrtName()
        {
            return mSharedPrefs.GetString(PREFERENCE_FIRST_NAME, "");
        }

        public static string GetLastName()
        {
            return mSharedPrefs.GetString(PREFERENCE_LAST_NAME, "");
        }

        public static string GetImagePath()
        {
            return mSharedPrefs.GetString(PREFERENCE_IMAGE_PATH, "");
        }

        public static void SaveFirstName(string firstName)
        {
            mPrefsEditor.PutString(PREFERENCE_FIRST_NAME, firstName);
            mPrefsEditor.Commit();
        }

        public static void SaveLastName(string lastName)
        {
            mPrefsEditor.PutString(PREFERENCE_LAST_NAME, lastName);
            mPrefsEditor.Commit();
        }

        public static void SaveImagePath(string imagePath)
        {
            mPrefsEditor.PutString(PREFERENCE_IMAGE_PATH, imagePath);
            mPrefsEditor.Commit();
        }

        public static void SaveAccessToken(string token)
        {
            mPrefsEditor.PutString(PREFERENCE_ACCESS_TOKEN, token);
            mPrefsEditor.Commit();
        }

        public static string GetAccessToken()
        {
            return mSharedPrefs.GetString(PREFERENCE_ACCESS_TOKEN, "");
        }
    }
}