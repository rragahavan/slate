using Android.App;
using Android.Runtime;
using Slate.Services;
using Slate.ViewModels;
using System;

namespace Slate.Droid.Classes
{
    public class AppContext : Application
    {
        [Application]
        public class Application : Android.App.Application
        {
            public Application(IntPtr javaReference, JniHandleOwnership transfer)
                : base(javaReference, transfer)
            {

            }

            public override void OnCreate()
            {
                base.OnCreate();
                ServiceContainer.Register<ILoginService>(() => new LoginService());
                ServiceContainer.Register(() => new LoginViewModel());
                ServiceContainer.Register<IEmployeeList>(() => new EmployeeListService());
                ServiceContainer.Register(() => new EmployeeListViewModel());
            }


        }
    }
}