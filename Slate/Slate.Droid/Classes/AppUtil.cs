using Android.App;
using Android.Content;
using Android.Views;

namespace Slate.Droid.Classes
{
    public static class AppUtil
    {
        public static bool IsNetworkAvailable(Context context)
        {
            Android.Net.ConnectivityManager connectivityManager = (Android.Net.ConnectivityManager)context.GetSystemService(Context.ConnectivityService);
            Android.Net.NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;
            return (activeConnection != null) && activeConnection.IsConnected;
        }

        public static void DialogBox(Context context, System.Type nextPage, Activity activity)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.SetTitle(Resource.String.confirm_log_out);
            alert.SetMessage(Resource.String.confirm_log_out_enquiry);
            alert.SetPositiveButton(Resource.String.Yes, (senderAlert, args) =>
            {
                AppPreferences.SaveAccessToken("");
                Intent i = new Intent(context, nextPage);
                context.StartActivity(i);
                ((Activity)context).Finish();

            });
            alert.SetNegativeButton(Resource.String.No, (senderAlert, args) =>
            {
                CustomToast customMessage = new CustomToast(context, activity, context.GetString(Resource.String.CancelLogout), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();

            });
            Dialog dialog = alert.Create();
            dialog.Show();
        }
    }
}