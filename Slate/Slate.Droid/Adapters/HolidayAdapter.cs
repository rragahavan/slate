using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Slate.Models;
using System;
using System.Collections.Generic;

namespace Slate.Droid.Adapters
{
    public class HolidayAdapter : RecyclerView.Adapter
    {
        public event EventHandler<int> ItemClick;
        public List<Holiday> holidays;
        public Context context;

        public HolidayAdapter(List<Holiday> holidays, Context context)
        {
            this.holidays = holidays;
            this.context = context;
        }


        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.RecyclerHolidayItems, parent, false);
            HolidayViewHolder vh = new HolidayViewHolder(itemView, OnClick);
            return vh;
        }

        public override int GetItemViewType(int position)
        {
            return position;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            HolidayViewHolder vh = holder as HolidayViewHolder;
            if (position % 2 != 0)
            {
                vh.LinearOneLayout.Visibility = ViewStates.Gone;
                vh.LinearTwoLayout.Visibility = ViewStates.Visible;
            }
            var emp = holidays[position];
            vh.HolidayName.Text = emp.Name;
            vh.HolidayMonth.Text = Convert.ToDateTime(emp.Date).ToString("MMMM");
            vh.Weekday.Text = Convert.ToDateTime(emp.Date).DayOfWeek.ToString();
            vh.HolidayDay1.Text = Convert.ToDateTime(emp.Date).Day.ToString();
            vh.HolidayMonth1.Text = Convert.ToDateTime(emp.Date).ToString("MMMM");
            vh.HolidayDay.Text = Convert.ToDateTime(emp.Date).Day.ToString();
            vh.HolidayType.Text = emp.Type;
        }

        public override int ItemCount
        {
            get { return holidays.Count; }
        }

        void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }
    }

    public class HolidayViewHolder : RecyclerView.ViewHolder
    {
        public TextView HolidayName { get; private set; }
        public TextView HolidayDay { get; private set; }
        public TextView HolidayMonth { get; private set; }
        public TextView Weekday { get; private set; }
        public TextView HolidayType { get; private set; }
        public LinearLayout LinearOneLayout { get; private set; }
        public LinearLayout LinearTwoLayout { get; private set; }
        public TextView HolidayDay1 { get; private set; }
        public TextView HolidayMonth1 { get; private set; }

        public HolidayViewHolder(View itemView, Action<int> listener)
            : base(itemView)
        {
            HolidayName = itemView.FindViewById<TextView>(Resource.Id.holidayNameText);
            HolidayDay = itemView.FindViewById<TextView>(Resource.Id.holidayDayText);
            HolidayMonth = itemView.FindViewById<TextView>(Resource.Id.holidayMonthText);
            Weekday = itemView.FindViewById<TextView>(Resource.Id.weekdayText);
            HolidayDay1 = itemView.FindViewById<TextView>(Resource.Id.holidayDayText1);
            HolidayMonth1 = itemView.FindViewById<TextView>(Resource.Id.holidayMonthText1);
            LinearOneLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearLayoutOne);
            LinearTwoLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearLayoutTwo);
            HolidayType = itemView.FindViewById<TextView>(Resource.Id.holidayTypeText);
            itemView.Click += (sender, e) => listener(base.AdapterPosition);
        }
    }
}