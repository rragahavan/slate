using Android.Support.V4.App;
using Android.Views;
using Slate.Droid.Fragments;
using Slate.Models;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Adapters
{
    public class SpotlightAdapter : FragmentPagerAdapter
    {
        private readonly List<Fragment> CardsFragments;

        public SpotlightAdapter(FragmentManager fm, List<SpotlightEmployee> spotlightEmployees, ContextThemeWrapper context) : base(fm)
        {
            CardsFragments = new List<Fragment>();
            foreach (var spotlightEmployee in spotlightEmployees)
                CardsFragments.Add(new SpotlightFragment(spotlightEmployee));

        }


        public override int Count
        {
            get
            {
                return CardsFragments.Count;
            }
        }


        public override Fragment GetItem(int position)
        {
            return CardsFragments[position];
        }
    }
}
