using Android.Content;
using Android.Runtime;
using Android.Support.V4.App;
using Java.Lang;
using Slate.Droid.Fragments;

namespace Slate.Droid.Adapters
{
    public class LeavePagerAdapter : FragmentPagerAdapter
    {
        private readonly string[] tabTitles;
        private readonly Context context;

        public LeavePagerAdapter(FragmentManager fm, string[] tabTitles, Context context) : base(fm)
        {
            this.tabTitles = tabTitles;
            this.context = context;
        }

        public override int Count
        {
            get
            {
                return tabTitles.Length;
            }
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {
            return CharSequence.ArrayFromStringArray(tabTitles)[position];
        }

        public override Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    return new MyLeaveFragment();
                case 1:
                    return new ApplyLeaveFragment();
                case 2:
                    return new LeaveSummaryFragment();
                case 3:
                    return new LeaveRequestFragment();
                default:
                    return null;
            }
        }
    }
}