
using Android.Content;
using Android.Runtime;
using Android.Support.V4.App;
using Java.Lang;
using Slate.Droid.Fragments;

namespace Slate.Droid.Adapters
{
    public class TimesheetAdapter : FragmentPagerAdapter
    {
        private readonly string[] tabTitles;
        private readonly Context context;

        public TimesheetAdapter(FragmentManager fm, string[] tabTitles, Context context) : base(fm)
        {
            this.tabTitles = tabTitles;
            this.context = context;
        }

        public override int Count
        {
            get
            {
                return tabTitles.Length;
            }
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {
            return CharSequence.ArrayFromStringArray(tabTitles)[position];
        }

        public override Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    return new TimesheetEntryFragment();
                case 1:
                    return new TimesheetDisplayFragment();
                default:
                    return null;
            }
        }

    }
}