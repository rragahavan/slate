using Android.Support.V4.App;
using Android.Views;
using Slate.Droid.Fragments;
using Slate.Models;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Adapters
{
    public class AnniversaryPagerAdapter : FragmentPagerAdapter
    {
        private readonly List<Fragment> CardsFragments;
        public AnniversaryPagerAdapter(FragmentManager fm, List<UpcomingAnniversary> upcomingAnniversary, ContextThemeWrapper context) : base(fm)
        {
            CardsFragments = new List<Fragment>();
            foreach (var upComingAnniversary in upcomingAnniversary)
                CardsFragments.Add(new ViewPagerFragment(upComingAnniversary));
        }
        public override int Count
        {
            get
            {
                return CardsFragments.Count;
            }
        }

        public override Fragment GetItem(int position)
        {
            return CardsFragments[position];
        }

    }
}