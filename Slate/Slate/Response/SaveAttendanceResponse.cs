﻿namespace Slate.Response
{
    public class Data
    {
        public int ok { get; set; }
        public int nModified { get; set; }
        public int n { get; set; }
    }
    public class SaveAttendanceResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
    }


}
