﻿namespace Slate.Response
{
    public class SaveTimesheetResponse
    {
        public bool success { get; set; }
        public TimesheetData data { get; set; }

    }
    public class TimesheetData
    {
        public int ok { get; set; }
        public int n { get; set; }
    }
}

