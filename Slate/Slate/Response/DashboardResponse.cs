﻿using Newtonsoft.Json;
using Slate.Models;

namespace Slate.Response
{
    public class DashboardResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public Dashboard Data { get; set; }
    }
}
