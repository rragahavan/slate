﻿using Newtonsoft.Json;
using Slate.Models;
using System.Collections.Generic;

namespace Slate.Response
{
    public class ApiResponse<T> where T : new()
    {
        //List<Error> _errors;

        //[JsonProperty("errors")]
        //public List<Error> Errors
        //{
        //    get
        //    {
        //        if (_errors == null)
        //            _errors = new List<Error>();
        //        return _errors;
        //    }
        //    set
        //    {
        //        _errors = value;
        //    }
        //}
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public T Data { get; set; }

       

    }

}
