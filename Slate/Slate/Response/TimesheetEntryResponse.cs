﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Slate.Response
{

    public class TimesheetUser
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("empcode")]
        public string Empcode { get; set; }
        [JsonProperty("firstname")]
        public string Firstname { get; set; }
        [JsonProperty("lastname")]
        public string Lastname { get; set; }
    }

    public class TimesheetEntry
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("taskDate")]
        public string TaskDate { get; set; }
        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
        [JsonProperty("task")]
        public string Task { get; set; }
        [JsonProperty("hours")]
        public string Hours { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("dateCreated")]
        public string DateCreated { get; set; }
        [JsonProperty("managerId")]
        public string ManagerId { get; set; }
        [JsonProperty("user")]
        public TimesheetUser User { get; set; }
    }
    public class TimesheetEntryResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public List<TimesheetEntry> Data { get; set; }

    }
}
