﻿using Slate.Models;

namespace Slate.Response
{

    public class TimesheetResponse
    {
        public bool success { get; set; }
        public TimesheetRequest data { get; set; }
    }
}
