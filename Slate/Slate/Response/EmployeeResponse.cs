﻿using Newtonsoft.Json;
using Slate.Models;

namespace Slate.Response
{
    public class EmployeeResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public Employee Data { get; set; }
    }
}
