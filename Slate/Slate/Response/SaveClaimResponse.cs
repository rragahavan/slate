﻿using Newtonsoft.Json;

namespace Slate.Response
{
    public class SaveClaimResponse
    {
        [JsonProperty("success")]
        public bool success { get; set; }
        [JsonProperty("data")]
        public Data data { get; set; }
    }
}
