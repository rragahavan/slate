﻿using Slate.Models;
using System.Collections.Generic;

namespace Slate.Response
{
    public class SettingsResponse
    {
        public bool success { get; set; }
        public List<Settings> data { get; set; }
    }
}
