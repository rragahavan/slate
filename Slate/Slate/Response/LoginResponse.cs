﻿using Newtonsoft.Json;
using Slate.Models;

namespace Slate.Response
{
    public class LoginResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public Login Data { get; set; }

    }
}
