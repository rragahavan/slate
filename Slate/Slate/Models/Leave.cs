﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Slate.Models
{
    public class UserAttendance1
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("empCode")]
        public string EmpCode { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }

    public class LeaveRequest
    {
        //[JsonProperty("_id")]
        public string id { get; set; }
        // [JsonProperty("fromDate")]
        public string fromDate { get; set; }
        // [JsonProperty("toDate")]
        public string toDate { get; set; }
        // [JsonProperty("numDays")]
        public int numDays { get; set; }
        // [JsonProperty("leaveType")]
        public string leaveType { get; set; }
        [JsonProperty("userComments")]
        public string UserComments { get; set; }
        //[JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("dateCreated")]
        public string DateCreated { get; set; }
        [JsonProperty("dateModified")]
        public string DateModified { get; set; }
        //[JsonProperty("managerComments")]
        public string managerComments { get; set; }
        [JsonProperty("user")]
        public UserAttendance1 User { get; set; }
        [JsonProperty("managerId")]
        public string ManagerId { get; set; }
        public string template { get; set; }
        public string _id { get; set; }
    }

    public class UserAttendance2
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("empCode")]
        public string EmpCode { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }

    public class ManagerleaveRequest
    {
        //[JsonProperty("_id")]
        public string _id { get; set; }
        // [JsonProperty("fromDate")]
        public string fromDate { get; set; }
        //[JsonProperty("toDate")]
        public string toDate { get; set; }
        //[JsonProperty("numDays")]
        public int numDays { get; set; }
        //[JsonProperty("leaveType")]
        public string leaveType { get; set; }
        //[JsonProperty("userComments")]
        public string userComments { get; set; }
        //[JsonProperty("status")]
        public string status { get; set; }
        //[JsonProperty("dateCreated")]
        public string dateCreated { get; set; }
        //[JsonProperty("dateModified")]
        public string dateModified { get; set; }
        //[JsonProperty("managerComments")]
        public string managerComments { get; set; }
        public UserAttendance2 User { get; set; }
        //[JsonProperty("managerId")]
        public string managerId { get; set; }
    }

    public class LeaveType
    {
        public string name { get; set; }
        public string Description { get; set; }
    }
    public class Leavesummary2
    {
        public LeaveType leaveType { get; set; }
        public int total { get; set; }
        public int balance { get; set; }
    }

    public class LeaveSummary
    {
        public string _id { get; set; }
        public UserAttendance2 user { get; set; }
        public List<Leavesummary2> leavesummary { get; set; }
    }


    public class Leave
    {
        [JsonProperty("leaveRequests")]
        public List<LeaveRequest> LeaveRequests { get; set; }
        [JsonProperty("managerleaveRequests")]
        public List<ManagerleaveRequest> ManagerleaveRequests { get; set; }
        public LeaveSummary leaveSummary { get; set; }
    }
}
