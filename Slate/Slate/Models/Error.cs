﻿using Newtonsoft.Json;

namespace Slate.Models
{
    public class Error
    {
        [JsonProperty("property")]
        public string Property { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

    }
}
