﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Slate.Models
{

    public class RecentEvent
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("eventDate")]
        public string EventDate { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("eventCreated")]
        public string EventCreated { get; set; }
    }

    public class UpcomingEvent
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("eventDate")]
        public string EventDate { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("eventCreated")]
        public string EventCreated { get; set; }
    }

    public class SpotlightEmployee
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("jobTitle")]
        public string JobTitle { get; set; }
        [JsonProperty("empCode")]
        public string EmpCode { get; set; }
        [JsonProperty("profileImage")]
        public string ProfileImage { get; set; }
        [JsonProperty("spotLightNotes")]
        public string SpotLightNotes { get; set; }
    }

    public class NewEmployee
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("jobTitle")]
        public string JobTitle { get; set; }
        [JsonProperty("joiningDate")]
        public string JoiningDate { get; set; }
        [JsonProperty("empCode")]
        public string EmpCode { get; set; }
        [JsonProperty("profileImage")]
        public string ProfileImage { get; set; }
    }

    public class UpcomingHoliday
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("date")]
        public string Date { get; set; }
    }

    public class UpcomingBirthday
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("birthDate")]
        public string BirthDate { get; set; }
        [JsonProperty("profileImage")]
        public string ProfileImage { get; set; }
        [JsonProperty("daysTillbirthDate")]
        public int DaysTillbirthDate { get; set; }
    }

    public class UpcomingAnniversary
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("joiningDate")]
        public string JoiningDate { get; set; }
        [JsonProperty("profileImage")]
        public string ProfileImage { get; set; }
        [JsonProperty("daysTilljoingDate")]
        public int DaysTilljoingDate { get; set; }
    }

    public class Dashboard
    {
        [JsonProperty("_id")]
        public int Id { get; set; }
        [JsonProperty("recentEvents")]
        public List<RecentEvent> RecentEvents { get; set; }
        [JsonProperty("upcomingEvents")]
        public List<UpcomingEvent> UpcomingEvents { get; set; }
        [JsonProperty("spotlightEmployees")]
        public List<SpotlightEmployee> SpotlightEmployees { get; set; }
        [JsonProperty("newEmployees")]
        public List<NewEmployee> NewEmployees { get; set; }
        [JsonProperty("upcomingHolidays")]
        public List<UpcomingHoliday> UpcomingHolidays { get; set; }
        [JsonProperty("upcomingBirthdays")]
        public List<UpcomingBirthday> UpcomingBirthdays { get; set; }
        [JsonProperty("upcomingAnniversaries")]
        public List<UpcomingAnniversary> UpcomingAnniversaries { get; set; }
    }
}
