﻿using Newtonsoft.Json;
using System;

namespace Slate.Models
{
    public class Timesheet
    {
        [JsonProperty("taskDate")]
        public string taskDate { get; set; }
        [JsonProperty("projectName")]
        public string projectName { get; set; }
        [JsonProperty("task")]
        public string task { get; set; }
        [JsonProperty("hours")]
        public string hours { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
    }

    public class TimesheetDateEntry
    {
        public string DayName { get; set; }
        public string Month { get; set; }
        public string DayNumber { get; set; }
        public DateTime Date { get; set; }

    }

}
