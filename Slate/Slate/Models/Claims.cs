﻿namespace Slate.Models
{
    public class Claims
    {
        public FinanceManager financeManager { get; set; }
        //[JsonProperty("date")]
        public string date { get; set; }
        public string template { get; set; }
        //[JsonProperty("category")]
        public string category { get; set; }
        // [JsonProperty("amount")]
        public int amount { get; set; }
        //[JsonProperty("employeeComments")]
        public string employeeComments { get; set; }
        //[JsonProperty("attachment")]
        public string attachment { get; set; }
        //[JsonProperty("currency")]
        public string currency { get; set; }
        // [JsonProperty("status")]
        public string status { get; set; }
    }
}
