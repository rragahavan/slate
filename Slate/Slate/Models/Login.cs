﻿using Newtonsoft.Json;

namespace Slate.Models
{
    public class Login
    {
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("user")]
        public Employee User { get; set; }
    }
}
