﻿using System.Collections.Generic;
using System.Net;

namespace Slate.Models
{
    public class BaseModel
    {
        private List<Error> _Errors;

        public BaseModel() { }

        public BaseModel(List<Error> errors, HttpStatusCode statusCode)
        {
            Errors = errors;
            StatusCode = statusCode;
        }

        public List<Error> Errors
        {
            get
            {
                if (_Errors == null)
                {
                    return new List<Error>();
                }
                return _Errors;
            }
            set
            {
                _Errors = value;
            }
        }
        public string ErrorMessage
        {
            get
            {
                if (Errors.Count == 0)
                    return string.Empty;
                return Errors[0].Message;

            }
        }
        public HttpStatusCode StatusCode { get; set; }

        public bool HasErrors => Errors?.Count > 0 || StatusCode != HttpStatusCode.OK;

    }
}
