﻿using System.Collections.Generic;

namespace Slate.Models
{

    public class User
    {
        public string _id { get; set; }
        public string empcode { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
    }

    public class Task
    {
        public string _id { get; set; }
        public string taskDate { get; set; }
        public string projectName { get; set; }
        public string task { get; set; }
        public string hours { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public string dateCreated { get; set; }
        public string managerId { get; set; }
        public User user { get; set; }
    }

    public class User2
    {
        public string _id { get; set; }
        public string empcode { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
    }

    public class MyTimesheetRequest
    {
        public string _id { get; set; }
        public List<Task> tasks { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int totalHours { get; set; }
        public string mailTemplate { get; set; }
        public User2 user { get; set; }
        public string status { get; set; }
        public string dateCreated { get; set; }
        public string dateModified { get; set; }
        public string managerId { get; set; }
    }

    public class TimesheetRequest
    {
        public string _id { get; set; }
        public List<Task> tasks { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int totalHours { get; set; }
        public string mailTemplate { get; set; }
        public User2 user { get; set; }
        public string status { get; set; }
        public string dateCreated { get; set; }
        public string dateModified { get; set; }
        public string managerId { get; set; }

    }
}
