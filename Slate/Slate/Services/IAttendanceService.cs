﻿using Slate.Models;
using Slate.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Slate.Services
{
    public interface IAttendanceService
    {
        Task<ApiResponse<List<Attendance>>> GetAttendanceList(string token);

    }
}
