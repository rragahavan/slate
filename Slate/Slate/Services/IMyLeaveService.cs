﻿using Slate.Models;
using Slate.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slate.Services
{
    public interface IMyLeaveService
    {
        Task<ApiResponse<Leave>> GetLeaveDetails(string token);
  
        //{
        //    LeaveResponse leaveList;
        //    var response = InvokeApi();
        //    leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
        //    return leaveList;
        //}
    }
}
