﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Slate.Models;
using Slate.Response;
using Slate.Common;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using RestSharp.Portable.Authenticators;

namespace Slate.Services
{
    public class MyLeaveService : IMyLeaveService
    {
        async Task<ApiResponse<Leave>> IMyLeaveService.GetLeaveDetails(string token)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            string Authorization = "\"" + token + "\"";
            ServiceRequest serviceRequest = new ServiceRequest("lms", "getLeaves");
            var request = new RestRequest("api/invoke", Method.POST);
            client.IgnoreResponseStatusCode = true;
            request.AddHeader("Content-Type", "application/json");
            request.Parameters.Clear();
            client.AddDefaultParameter("Authorization", Authorization);
            request.AddJsonBody(serviceRequest);
            var response = await client.Execute<ApiResponse<Leave>>(request);
            return response.Data;
        }
    }
}
