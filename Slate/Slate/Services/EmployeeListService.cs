﻿using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using Slate.Common;
using Slate.Response;
using System;
using System.Text;
using System.Threading.Tasks;


namespace Slate.Services
{
    public class EmployeeListService : IEmployeeList
    {

        async Task<ApiResponse<EmployeeListData>> IEmployeeList.GetEmployeeList(int page, string token)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));

            var parameter = new { filter = "Active", page = page };
            ServiceRequest serviceRequest = new ServiceRequest("hrms", "employeelist", parameter);
            var request = new RestRequest("api/invoke", Method.POST);
            client.IgnoreResponseStatusCode = true;
            string Authorization = "\"" +token + "\"";
            request.AddHeader("Authorization", Authorization);
            request.Parameters.Clear();
            request.AddJsonBody(serviceRequest);
            var response = await client.Execute<ApiResponse<EmployeeListData>>(request);
            return response.Data;
        }
    }
}
