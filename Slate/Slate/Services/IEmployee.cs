﻿using Slate.Models;
using Slate.Response;
using System.Threading.Tasks;

namespace Slate.Services
{
    public interface IEmployee
    {
        Task<ApiResponse<Employee>> GetEmployee(string token, string empCode);
    }
}
