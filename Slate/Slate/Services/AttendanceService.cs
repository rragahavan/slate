﻿using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using Slate.Common;
using Slate.Models;
using Slate.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Slate.Services
{
    public class AttendanceService : IAttendanceService
    {
        async Task<ApiResponse<List<Attendance>>> IAttendanceService.GetAttendanceList(string token)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            string Authorization = "\"" + token + "\"";
            ServiceRequest serviceRequest = new ServiceRequest("hrms", "getAttendanceList");
            var request = new RestRequest("api/invoke", Method.POST);
            client.IgnoreResponseStatusCode = true;
            request.AddHeader("Content-Type", "application/json");
            request.Parameters.Clear();
            client.AddDefaultParameter("Authorization", Authorization);
            request.AddJsonBody(serviceRequest);
            var response = await client.Execute<ApiResponse<List<Attendance>>>(request);
            return response.Data;
        }
    }
}
