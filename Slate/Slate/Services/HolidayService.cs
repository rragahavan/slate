﻿using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using Slate.Common;
using Slate.Models;
using Slate.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Slate.Services
{
    public class HolidayService : ISettingsService
    {
        async Task<ApiResponse<List<Settings>>> ISettingsService.GetSetting(string token)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            ServiceRequest serviceRequest = new ServiceRequest("hrms", "settings");
            var request = new RestRequest("api/invoke", Method.POST);
            string str = "\""+token+"\"";
            string Authorization = "\"" + token + "\"";
            request.AddHeader("Authorization", Authorization);
            request.Parameters.Clear();
            request.AddJsonBody(serviceRequest);
            var response = await client.Execute<ApiResponse<List<Settings>>>(request);
            return response.Data;
        }

    }
}
