﻿using Slate.Response;
using System.Threading.Tasks;

namespace Slate.Services
{
    public interface IEmployeeList
    {
        Task<ApiResponse<EmployeeListData>> GetEmployeeList(int page,string token);
    }
}
