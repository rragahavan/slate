﻿using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using Slate.Common;
using Slate.Models;
using Slate.Response;
using System;
using System.Threading.Tasks;

namespace Slate.Services
{
    public class LoginService : ILoginService
    {
        async Task<ApiResponse<Login>> ILoginService.Login(string  accessToken)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            var parameter = new { token = "\"" + accessToken + "\"" };
            var request = new RestRequest("api/login", Method.POST);
            request.AddJsonBody(parameter);
            var response = await client.Execute<ApiResponse<Login>>(request);
            return response.Data;
        }
    }

}

