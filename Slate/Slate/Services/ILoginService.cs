﻿using Slate.Models;
using Slate.Response;
using System.Threading.Tasks;

namespace Slate.Services
{
    public interface ILoginService
    {
        Task<ApiResponse<Login>> Login(string token);
        
    }
}
