﻿using RestSharp.Portable;
using RestSharp.Portable.Authenticators;
using RestSharp.Portable.HttpClient;
using Slate.Common;
using Slate.Models;
using Slate.Response;
using System;
using System.Threading.Tasks;

namespace Slate.Services
{
    public class EmployeeService : IEmployee
    {
        async Task<ApiResponse<Employee>> IEmployee.GetEmployee(string token, string empCode)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            var parameter = new { empId = empCode };
            ServiceRequest serviceRequest = new ServiceRequest("hrms", "getemployee", parameter);
            var request = new RestRequest("api/invoke", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            string Authorization = "\"" + token + "\"";
            client.IgnoreResponseStatusCode = true;
            request.AddHeader("Content-Type", "application/json");
            request.Parameters.Clear();
            client.AddDefaultParameter("Authorization", Authorization);
            request.AddJsonBody(serviceRequest);
            var response = await client.Execute<ApiResponse<Employee>>(request);
           return response.Data;
        }
    }
}
