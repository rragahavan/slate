﻿using Slate.Models;
using Slate.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slate.Services
{
    public interface IDashboardService
    {
        Task<ApiResponse<Dashboard>> GetDashboardDetails(string token);   
    }
}
