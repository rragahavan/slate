﻿using Slate.Models;
using Slate.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Slate.Services
{
    public interface ISettingsService
    {
        Task<ApiResponse<List<Settings>>> GetSetting(string token);
    }
}
