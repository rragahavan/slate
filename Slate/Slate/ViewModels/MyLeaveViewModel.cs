﻿using Slate.Models;
using Slate.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slate.ViewModels
{
    public class MyLeaveViewModel:BaseViewModel
    {
        private readonly IMyLeaveService myLeaveService;
        public MyLeaveViewModel()
        {
            myLeaveService = new MyLeaveService(); 
        }

        public async Task<Leave> GetLeaveDetails(string token)
        {

            var c = await myLeaveService.GetLeaveDetails(token);
            if (c.Success)
            {
                return c.Data;
            }
            return null;
        }

    }
}
