﻿using Slate.Models;
using Slate.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slate.ViewModels
{
    public class DashboardViewModel:BaseViewModel
    {
        private readonly IDashboardService dashboardService;
        public DashboardViewModel()
        {
            dashboardService = new DashboardService();
        }
        public async Task<Dashboard> GetSettings(string token)
        {
            var c = await dashboardService.GetDashboardDetails(token);
            if (c.Success)
            {
                return c.Data;
            }
            return null;
        }

    }
}
