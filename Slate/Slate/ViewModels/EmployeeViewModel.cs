﻿using Slate.Models;
using Slate.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slate.ViewModels
{
    public class EmployeeViewModel:BaseViewModel 
    {
        private readonly IEmployee employeeService;
        public EmployeeViewModel()
        {
            employeeService = new EmployeeService();
        }
        public async Task<Employee> EmployeeList(string token, string empCode)
        {

            var c = await employeeService.GetEmployee(token, empCode);
            if (c.Success)
            {
                return c.Data;
            }
            return null;
        }

    }
}
