﻿using Slate.Models;
using Slate.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slate.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        private readonly ISettingsService settingsService;
        public SettingsViewModel()
        {
            settingsService = new HolidayService();
        }
        public async Task<Settings> GetSettings(string token)
        {
            var c = await settingsService.GetSetting(token);
            if (c.Success)
            {
                return c.Data[0];
            }
            return null;
        }

    }
}
