﻿using Slate.Models;
using Slate.Response;
using Slate.Services;
using System.Linq;
using System.Threading.Tasks;

namespace Slate.ViewModels
{
    public class LoginViewModel
    {
        private readonly ILoginService loginService;
        public LoginViewModel()
        {
            loginService = ServiceContainer.Resolve<ILoginService>();
        }
        public async Task<Login> Login(string token)
        {
            var c = await loginService.Login(token);
            if (c.Success)
            {
                return c.Data;
            }
            return null;
        }

    }
}
