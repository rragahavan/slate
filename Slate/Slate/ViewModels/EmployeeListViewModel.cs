﻿using Slate.Response;
using Slate.Services;

using System.Threading.Tasks;

namespace Slate.ViewModels
{
    public class EmployeeListViewModel : BaseViewModel
    {
        private readonly IEmployeeList employeeListService;
        public EmployeeListViewModel()
        {
            employeeListService = ServiceContainer.Resolve<IEmployeeList>();
        }
        public async Task<EmployeeListData> EmployeeList(int page,string token)
        {
            
            var c = await employeeListService.GetEmployeeList(page, token);
            if (c.Success)
            {
                return c.Data;
            }
            return null;
        }
    }
}
