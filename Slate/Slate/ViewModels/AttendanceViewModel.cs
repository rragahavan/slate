﻿using Slate.Models;
using Slate.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Slate.ViewModels
{
    public class AttendanceViewModel : BaseViewModel
    {
        private readonly IAttendanceService attendanceService;
        public AttendanceViewModel()
        {
            attendanceService = new AttendanceService();
        }

        public async Task<List<Attendance>> GetAttendance(string token)
        {
            var c = await attendanceService.GetAttendanceList(token);
            if (c.Success)
            {
                return c.Data;
            }
            return null;
        }

    }
}
