﻿using Slate.Models;
using System.Collections.Generic;

namespace Slate.ViewModels
{
    public class BaseViewModel
    {
        public List<Error> Errors { get; set; }

        public bool HasErrors
        {
            get
            {
                return this.Errors != null && this.Errors.Count > 0;
            }
        }
        public string ErrorMessage
        {
            get
            {
                if (!HasErrors)
                    return string.Empty;

                return Errors[0].Message;

            }

        }
        public string Success { get; }


    }
}
